% Copyright 20xx - 2019. Duke University
function [profiles] = EdgeAcutance2D(varargin)
%This function computes the edge profile acutance of a segmented object. It
%works by getting line profiles normal to the edge of the object around the
%object's perimeter. It only gets profiles around the perimeter at
%specified polar angles. Thus the more angles, the more profiles will be
%returned. It also shifts the profiles such that they are given as a
%distance from the edge of the object. For a given profile, the edge is
%defined as the point at which the pixel value is (max-min)/2+min.
%--------------------------------------------------------------------------
%Inputs:
%   I          Input image
%   BW         Input  binary mask (segmentation)
%   nP         Desired length of line profiles (in pixels)
%   Th         Scalar or vector designating the desired angular sampling
%   MakePlot   Boolean controls if a figure with plots is made (default 0)
%--------------------------------------------------------------------------
%Outputs:
%   profiles   Structured variable with the measured profiles
%                  profiles.d  = vector of distance values
%                  profiles.c  = vector of pixel values
%                  profiles.xi = staring x coordinates of profile
%                  profiles.yi = staring y coordinates of profile
%              Note that the length of profiles is equal to the number of
%              angular samples (i.e., Th or length(Th)). To visualize
%              profiles(i), just call:
%                  plot(profiles(i).d,profiles(i).c)
%--------------------------------------------------------------------------         
%Syntax:
%   profiles=EdgeAcutance2D(I,BW) takes the input image, I and a
%   binary mask, BW, and returns a the structured variable, profiles.
%   size(I)=size(BW)
%
%   profiles=EdgeAcutance2D(I,BW,nP) lets you specify the approximate
%   length (in pixels) of the profiles, nP. (default is 5)
%
%   profiles=EdgeAcutance2D(I,BW,nP,Th) lets you control the angular
%   sampling (default is 10). If Th is a scalar, the function will use n=Th
%   evenly spaced angular samples to measure the edge profile. To specify
%   the desired sampling angles, just pass Th as a vector. Note that Th is
%   periodic -> 0<=Th<2*pi.
%
%   profiles=EdgeAcutance2D(I,BW,nP,Th,MakePlots) set MakePlots to 1
%   to have the function automatically display a plot of the profiles.
%   Default is 0, i.e., no plots.
%--------------------------------------------------------------------------
%Notes:
%   Written by Justin Solomon, 5 Nov. 2012.
%
%   Everyone has permission to use without asking.
%
%   Current version: 1.1, updated 7 Nov. 2012.
%
%   Created in MATLAB_R2012a
%
%   Requires the Image Processing Toolbox
%
%   Loosely based on: R. M. Rangayyan, "Algorithm for the computation
%   of region-based image edge profile actuance", J of Electronic Imaging,
%   Vol. 4(1), Jan. 1995.
%--------------------------------------------------------------------------

%Check inputs and set defaults
switch nargin
    case 2  %profiles=EdgeAcutance(I,BW)
        I=varargin{1}; BW=varargin{2}; nP=5; dTh=(2*pi)/10; Th=0:dTh:2*pi-dTh;
        MakePlot=0;
    case 3  %profiles=EdgeAcutance(I,BW,nP)
        I=varargin{1}; BW=varargin{2}; nP=varargin{3}; dTh=(2*pi)/10; Th=0:dTh:2*pi-dTh;
        MakePlot=0;
    case 4  %profiles=EdgeAcutance(I,BW,nP,nTh)
        I=varargin{1}; BW=varargin{2}; nP=varargin{3}; Th=varargin{4};
        if length(Th)==1
            dTh=(2*pi)/Th; Th=0:dTh:2*pi-dTh;
        end
        MakePlot=0;
    case 5
        I=varargin{1}; BW=varargin{2}; nP=varargin{3}; Th=varargin{4};
        if length(Th)==1
            dTh=(2*pi)/Th; Th=0:dTh:2*pi-dTh;
        end
        MakePlot=varargin{5};
end
%--------------------------------------------------------------------------

%crop the image and mask for speed
buff=nP+1;
[Y, X]=find(BW);
I=I(min(Y)-buff:max(Y)+buff,min(X)-buff:max(X)+buff);
BW=BW(min(Y)-buff:max(Y)+buff,min(X)-buff:max(X)+buff);

%define a coordinate system centered at the center of mass of BW
x=1:size(BW,2); y=1:size(BW,1);
COM=regionprops(BW,'Centroid'); COM=round(COM.Centroid);
x=x-COM(1); y=y-COM(2);

%get the edge pixels of the mask and compute their polar coordinates
E=edge(BW);
[Yind Xind]=find(E);
B = bwtraceboundary(E,[Yind(1) Xind(1)],'N');
Yind=B(:,1); Xind=B(:,2);
[THedge RHO]=cart2pol(x(Xind),y(Yind));
THedge(THedge<0)=THedge(THedge<0)+2*pi;

%loop through Th to find the pixel of interest at each angle, get the
%normal line, and get the profile along that line.
for i=1:length(Th);
    [m ind]=min(abs(THedge-Th(i)));
    %These if statements are here to deal with being at the "end" or
    %"beginning" of the perimter of the mask.
    if ind-2>0 && ind+2<=length(Xind)
        Px=x(Xind(ind-2:ind+2)); Py=y(Yind(ind-2:ind+2));
    elseif ind==1
        Px=[x(Xind(end-2)) x(Xind(end-1)) x(Xind(1:3))];
        Py=[y(Yind(end-2)) y(Yind(end-1)) y(Yind(1:3))];
    elseif ind==2
        Px=[x(Xind(end-1)) x(Xind(1:4))];
        Py=[y(Yind(end-1)) y(Yind(1:4))];
    elseif ind==length(Xind)
        Px=[x(Xind(ind-2:ind)) x(Xind(1:2))];
        Py=[y(Yind(ind-2:ind)) y(Yind(1:2))];
    elseif ind==length(Xind)-1;
        Px=[x(Xind(ind-2:ind+1)) x(Xind(1))];
        Py=[y(Yind(ind-2:ind+1)) y(Yind(1))];
    end
    
    %define a translated and rotated coordinate system centered on the
    %pixel of interest
    xp=(Px-Px(3)).*cos(THedge(ind)-pi/2)+(Py-Py(3)).*sin(THedge(ind)-pi/2);
    yp=(Py-Py(3)).*cos(THedge(ind)-pi/2)-(Px-Px(3)).*sin(THedge(ind)-pi/2);
    
    %fit a line to the points in the rotated coordinate system
    p=polyfit(xp,yp,1); 
    
    %unit vector pointing in direction of slope (in rotated coordinate
    %system)
    up=1/sqrt(1+p(1).^2);  vp=p(1)./sqrt(1+p(1).^2);
    
    %rotate by 90 degrees to get the normal vector (still in the rotated
    %coordinate system)
    unorm=vp; vnorm=-up;
    
    %rotate to get back to image coordinate system
    u=unorm*cos(pi/2-THedge(ind))+vnorm*sin(pi/2-THedge(ind));
    v=-unorm*sin(pi/2-THedge(ind))+vnorm*cos(pi/2-THedge(ind));
    
    %define end points of profile
    P1=[Px(3) Py(3)]-(nP/2).*[u v];
    P2=[Px(3) Py(3)]+(nP/2).*[u v];
    
    %make sure P1 is in the mask and P2 is outside
    D1=norm(P1); %distance from center of lesion to P1
    D2=norm(P2); %distance from center of lesion to P2
    if D1 > D2
        temp=P1;
        P1=P2;
        P2=temp;
    end
    
    %use improfile() to get line profiles of image
    [cx,cy,c]=improfile(x,y,I,[P1(1) P2(1)],[P1(2) P2(2)],nP);
    d=sqrt((cx-cx(1)).^2+(cy-cy(1)).^2);
    [d] = dshift(d,c); %shift the profile so d=0 corresponds to the midway between min(c) and max(c)
    profiles(i).d=d; %d is the distance vector for the profile
    profiles(i).c=c; %c is the vector containing the pixel values
    profiles(i).xi=[P1(1) P2(1)];  %xi describes the staring and ending x coordinates of the profile
    profiles(i).yi=[P1(2) P2(2)];  %yi describes the staring and ending x coordinates of the profile
end

if MakePlot
    f=figure;
    subplot(2,1,1);
    imshow(mat2gray(I),'XData',x,'YData',y);
    hold on
    red = cat(3,ones(size(I)),zeros(size(I)),zeros(size(I)));
    h=imshow(red,'XData',x,'YData',y);
    set(h,'AlphaData',E.*.3);
    for i=1:length(profiles)
        plot(profiles(i).xi,profiles(i).yi,'-b.')
    end
    axis on;
    
    subplot(2,1,2);
    for i=1:length(profiles)
        plot(profiles(i).d,profiles(i).c,'-bo','MarkerEdgeColor','b','MarkerFaceColor','b')
        hold on
    end
    set(gca,'color',get(gcf,'color').*.95)
    box off; xlabel('Distance from edge (pixels)','FontSize',14); 
    ylabel('Pixel Values','FontSize',14)
end


end


function [dshifted] = dshift(d,HUs)
[HUmax imax]=max(HUs);
[HUmin imin]=min(HUs);
HUmid=((HUmax-HUmin)/2)+HUmin;
dsize=d(2)-d(1);
dfine=d(1):dsize/5:d(end);
HUfine=interp1(d,HUs,dfine);
[m i]=min(abs(HUfine-HUmid));
dmid=dfine(i);
dshifted=d-dmid;
end
