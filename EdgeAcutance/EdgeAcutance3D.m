% Copyright 20xx - 2019. Duke University
function [profiles] = EdgeAcutance3D(varargin)
%This function computes the edge profile acutance of a segmented object. It
%works by getting line profiles normal to the surface of the object around
%the object's perimeter. It only gets profiles around the perimeter at
%specified azimuth and elevation angles. Thus the more angles, the more
%profiles will be returned. It also shifts the profiles such that they are
%given as a distance from the edge of the object. For a given profile, the
%edge is defined as the point at which the pixel value is (max-min)/2+min.
%--------------------------------------------------------------------------
%Inputs:
%   I          Input image
%   BW         Input  binary mask (segmentation)
%   psize      Pixel size [x y z] default is [1 1 1]
%   nP         Desired length of line profiles (default is 5);
%   Az         List of azimuth angles (see cart2sph for definition)
%   El         List of elevation angles (see cart2sph for definition)
%   MakePlot   Boolean controls if a figure with plots is made (default 0)
%--------------------------------------------------------------------------
%Outputs:
%   profiles   Structured variable with the measured profiles
%                  profiles.d  = vector of distance values
%                  profiles.c  = vector of pixel values
%                  profiles.xi = staring x coordinates of profile
%                  profiles.yi = staring y coordinates of profile
%                  profiles.zi = staring z coordinates of profile
%              Note that the length of profiles is equal to the number of
%              angular samples (i.e., Th or length(Th)). To visualize
%              profiles(i), just call:
%                  plot(profiles(i).d,profiles(i).c)
%--------------------------------------------------------------------------         
%Syntax:
%   profiles=EdgeAcutance3D(I,BW) takes the input image, I and a
%   binary mask, BW, and returns a the structured variable, profiles.
%   size(I)=size(BW)
%
%   profiles=EdgeAcutance3D(I,BW,psize) lets you specify the pixel size in
%   each direction
%
%   profiles=EdgeAcutance3D(I,BW,psize,nP) lets you specify the approximate
%   length of the profiles, nP. (default is 5)
%
%   profiles=EdgeAcutance3D(I,BW,psize,nP,Az,El) lets you control the angular
%   sampling. Az and El are both vectors that let you specify the
%   directions (in spherical coordinates) where the profiles will be
%   calculated. Default is 82 profiles distributed at evenly distributed Az
%   and El angles.
%
%   profiles=EdgeAcutance3D(I,BW,psize,nP,Az,El,MakePlots) set MakePlots to 1
%   to have the function automatically display a plot of the profiles.
%   Default is 0, i.e., no plots.
%--------------------------------------------------------------------------
%Notes:
%   Written by Justin Solomon, 23 Dec. 2012.
%
%   Everyone has permission to use without asking.
%
%   Current version: 1.0
%
%   Created in MATLAB_R2012a
%
%   Requires the Image Processing Toolbox
%
%   Loosely based on: R. M. Rangayyan, "Algorithm for the computation
%   of region-based image edge profile actuance", J of Electronic Imaging,
%   Vol. 4(1), Jan. 1995.
%--------------------------------------------------------------------------

%Check inputs and set defaults
switch nargin
    case 2  %profiles=EdgeAcutance(I,BW)
        I=varargin{1}; BW=varargin{2}; psize=[1 1 1]; nP=5; dAz=(2*pi)/10; Az=0:dAz:2*pi-dAz;
        El=linspace(-pi/2,pi/2,10); [AZ EL]=meshgrid(Az,El); Az=zeros(82,1); El=zeros(82,1);
        temp=AZ(2:end-1,:); Az(1:80)=temp(:); Az(81)=AZ(1,1); Az(82)=AZ(end,1);
        temp=EL(2:end-1,:); El(1:80)=temp(:); El(81)=EL(1,1); El(82)=EL(end,1);
        MakePlot=0;
    case 3  %profiles=EdgeAcutance(I,BW,psize)
        I=varargin{1}; BW=varargin{2}; psize=varargin{3}; nP=5; dAz=(2*pi)/10; Az=0:dAz:2*pi-dAz;
        El=linspace(-pi/2,pi/2,10); [AZ EL]=meshgrid(Az,El); Az=zeros(82,1); El=zeros(82,1);
        temp=AZ(2:end-1,:); Az(1:80)=temp(:); Az(81)=AZ(1,1); Az(82)=AZ(end,1);
        temp=EL(2:end-1,:); El(1:80)=temp(:); El(81)=EL(1,1); El(82)=EL(end,1);
        MakePlot=0;
    case 4  %profiles=EdgeAcutance(I,BW,psize,nP)
        I=varargin{1}; BW=varargin{2}; psize=varargin{3}; nP=varargin{4}; dAz=(2*pi)/10; Az=0:dAz:2*pi-dAz;
        El=linspace(-pi/2,pi/2,10); [AZ EL]=meshgrid(Az,El); Az=zeros(82,1); El=zeros(82,1);
        temp=AZ(2:end-1,:); Az(1:80)=temp(:); Az(81)=AZ(1,1); Az(82)=AZ(end,1);
        temp=EL(2:end-1,:); El(1:80)=temp(:); El(81)=EL(1,1); El(82)=EL(end,1);
        MakePlot=0;
    case 6 %profiles=EdgeAcutance3D(I,BW,psize,nP,Az,El)
        I=varargin{1}; BW=varargin{2}; psize=varargin{3}; nP=varargin{4};
        Az=varargin{5}; El=varargin{6};
        MakePlot=0;
    case 7 %profiles=EdgeAcutance3D(I,BW,psize,nP,Az,El,MakePlots)
        I=varargin{1}; BW=varargin{2}; psize=varargin{3}; nP=varargin{4};
        Az=varargin{5}; El=varargin{6};
        MakePlot=varargin{7};
end
%--------------------------------------------------------------------------

%crop the image and mask for speed
buff=round(nP./psize)+1;
[Y, X, Z]=ind2sub(size(BW),find(BW));
%Check to make sure the cropped region doesn't go past limits of image
if min(X)-buff(1)>0
    minx=min(X)-buff(1);
else
    minx=1;
end
if min(Y)-buff(2)>0
    miny=min(Y)-buff(2);
else
    miny=1;
end
if min(Z)-buff(3)>0
    minz=min(Z)-buff(3);
else
    minz=1;
end
if max(X)+buff(1)<=size(I,2)
    maxx=max(X)+buff(1);
else
    maxx=size(I,2);
end
if max(Y)+buff(2)<=size(I,1)
    maxy=max(Y)+buff(2);
else
    maxy=size(I,1);
end
if max(Z)+buff(3)<=size(I,3)
    maxz=max(Z)+buff(3);
else
    maxz=size(I,3);
end
I=I(miny:maxy,minx:maxx,minz:maxz);
BW=BW(miny:maxy,minx:maxx,minz:maxz);

%define a coordinate system centered at the center of mass of BW
x=(0:size(BW,2)-1)*psize(1); 
y=(0:size(BW,1)-1)*psize(2);
z=(0:size(BW,3)-1)*psize(3);
COM=regionprops(BW,'Centroid'); COM=round(COM.Centroid); COM=[x(COM(1)) y(COM(2)) z(COM(3))];
x=x-COM(1); y=y-COM(2); z=z-COM(3); %Now the origin is at the center of mass of the lesion
[X Y Z]=meshgrid(x,y,z);

%get the edge pixels of the mask and compute their polar coordinates
M1=imdilate(BW,ones(3,3,3));
E=M1 & ~BW;
xi=X(E); yi=Y(E); zi=Z(E); %This is a list of coordinates of the edge voxels
[th phi r]=cart2sph(xi,yi,zi); th(th<0)=th(th<0)+2*pi;

%loop through Th to find the voxel of interest at each angle, get the
%normal line, and get the profile along that line.
for i=1:length(Az);
    [m ind]=min(sqrt((Az(i)-th).^2+(El(i)-phi).^2));
    
    %get edge voxels within a radius of nP around the voxel of interest
    rad=sqrt((xi(ind)-xi).^2+(yi(ind)-yi).^2+(zi(ind)-zi).^2);
    Pmask=rad<=max(psize)*3;
    
    %get voxels near the voxel of interest and define rotated coordinate
    %system
    dth=-th(ind); dphi=(pi/2)-phi(ind);
    xp=xi(Pmask); yp=yi(Pmask); zp=zi(Pmask);
    [xp yp zp]=rotatePoints(xp,yp,zp,dth,'z');
    [xp yp zp]=rotatePoints(xp,yp,zp,dphi,'x');
    
    %fit points to plane (in rotated coordinate system)
    [a b c nor]=fitplane(xp,yp,zp);
    
    %rotate the normal vector back to original coordinate system
    [u v w]=rotatePoints(nor(1),nor(2),nor(3),-dphi,'x');
    [u v w]=rotatePoints(u,v,w,-dth,'z');
    
    %define end points of profile
    P1=[xi(ind),yi(ind),zi(ind)]-(nP/2).*[u v w];
    P2=[xi(ind),yi(ind),zi(ind)]+(nP/2).*[u v w];
    
    %make sure P1 is in the mask and P2 is outside
    D1=norm(P1); %distance from center of lesion to P1
    D2=norm(P2); %distance from center of lesion to P2
    if D1 > D2
        temp=P1;
        P1=P2;
        P2=temp;
    end
    
    %use improfile() to get line profiles of image
    [cx cy cz c]=improfile3D(x,y,z,I,[P1(1) P2(1)],[P1(2) P2(2)],[P1(3) P2(3)],'linear');
    d=sqrt((cx-cx(1)).^2+(cy-cy(1)).^2+(cz-cz(1)).^2);
    [d] = dshift(d,c); %shift the profile so d=0 corresponds to the midway between min(c) and max(c)
    profiles(i).d=d; %d is the distance vector for the profile
    profiles(i).c=c; %c is the vector containing the pixel values
    profiles(i).xi=[P1(1) P2(1)];  %xi describes the staring and ending x coordinates of the profile
    profiles(i).yi=[P1(2) P2(2)];  %yi describes the staring and ending x coordinates of the profile
    profiles(i).zi=[P1(3) P2(3)];  %yi describes the staring and ending x coordinates of the profile
end

if MakePlot
    f=figure;
    subplot(2,1,1);
    p = patch(isosurface(x,y,z,BW,.1));
    set(p,'FaceColor',[.5 .5 .5],'EdgeColor','none');
    set(p,'FaceAlpha',.5);
     daspect(psize)
    view(3); %axis tight
    set(gca,'color',get(gcf,'color'))
    l=camlight;
    lighting gouraud
    axis off
    hold on
    for i=1:length(profiles)
        plot3(profiles(i).xi,profiles(i).yi,profiles(i).zi,'-r.')
        Xline(i)=profiles(i).xi(2); Yline(i)=profiles(i).yi(2); Zline(i)=profiles(i).zi(2);
    end
    axis off;
    %Fix this code to add an x,y,z axis
%     x=[-2 0 0]; y=[0 -2 0]; z=[0 0 -2]; u=[4 0 0]; v=[0 4 0]; w=[0 0 4];
%     quiver3(x,y,z,u,v,w,0,'k','LineStyle','-','ShowArrowHead','off','color',[.5 .5 .5])
%     text(2.2,0,0,'x','FontSize',Fsize,'HorizontalAlignment','center','color',[.5 .5 .5])
%     text(0,2.2,0,'y','FontSize',Fsize,'HorizontalAlignment','center','color',[.5 .5 .5])
%     text(0,0,2.2,'z','FontSize',Fsize,'HorizontalAlignment','center','color',[.5 .5 .5])
    
    subplot(2,1,2);
    for i=1:length(profiles)
        plot(profiles(i).d,profiles(i).c,'-bo','MarkerEdgeColor','b','MarkerFaceColor','b')
        hold on
    end
    set(gca,'color',get(gcf,'color').*.95)
    box on; xlabel('Distance from edge (pixel units)','FontSize',14); 
    ylabel('Pixel Values','FontSize',14)
end


end

%--------------------------------------------------------------------------
%Embedded Functions (click to expand)

function [dshifted] = dshift(d,HUs)
[HUmax imax]=max(HUs);
[HUmin imin]=min(HUs);
HUmid=((HUmax-HUmin)/2)+HUmin;
dsize=d(2)-d(1);
dfine=d(1):dsize/5:d(end);
HUfine=interp1(d,HUs,dfine);
[m i]=min(abs(HUfine-HUmid));
dmid=dfine(i);
dshifted=d-dmid;
end

function [a b c nor]=fitplane(x,y,z)
%equation of a plane z=ax+by+c (normal is (a,b,c))
const=ones(size(x));
A=[x y const];
X=A\z; a=X(1); b=X(2); c=X(3);

%get two vectors on plane
p1=[0 0 c]; p2=[1 0 a+c]; p3=[0 1 b+c];
v1=p2-p1; v2=p3-p1;
nor=cross(v1,v2);
nor=nor/norm(nor);
end

function [xrot yrot zrot]=rotatePoints(x,y,z,th,ax)

switch ax
    case 'x'
        rx=th; ry=0; rz=0;
    case 'y'
        rx=0; ry=th; rz=0;
    case 'z'
        rx=0; ry=0; rz=th;
end

rot=[1 0 0;0 cos(rx) -sin(rx);0 sin(rx) cos(rx)]*...
        [cos(ry) 0 sin(ry);0 1 0;-sin(ry) 0 cos(ry)]*...
        [cos(rz) -sin(rz) 0;sin(rz) cos(rz) 0;0 0 1];
    
drot=rot*[x y z]'; drot=drot';
xrot=drot(:,1); yrot=drot(:,2); zrot=drot(:,3);
end

function [cx cy cz c] = improfile3D(varargin)


%Check inputs and set defaults
switch nargin
    case 4  %improfile3D(I,xi,yi,zi)
        I=varargin{1}; xi=varargin{2}; yi=varargin{3}; zi=varargin{4};
        x=1:size(I,2); y=1:size(I,1); z=1:size(I,3);
        s=[1 1 1];
        method='linear';
    case 9  %improfile3D(x,y,z,I,xi,yi,zi,n,method)
        x=varargin{1}; y=varargin{2}; z=varargin{3}; I=varargin{4};
        xi=varargin{5}; yi=varargin{6}; zi=varargin{7}; s=round(varargin{8});
        method=varargin{9};
    case 8  %improfile3D(x,y,z,I,xi,yi,zi,n || method)
        x=varargin{1}; y=varargin{2}; z=varargin{3}; I=varargin{4};
        xi=varargin{5}; yi=varargin{6}; zi=varargin{7};
        if isa(varargin{8},'char')
            method=varargin{8};
            s=[1 1 1];
        else
            s=round(varargin{8});
            method='linear';
        end
end

%--------------------------------------------------------------------------

%Define a subsampled coordinates.
psize(1)=abs(x(2)-x(1));
psize(2)=abs(y(2)-y(1));
psize(3)=abs(z(2)-z(1));
nx=x(1):psize(1)/s(1):x(end);
ny=y(1):psize(2)/s(2):y(end);
nz=z(1):psize(3)/s(3):z(end);

%Use Bresenhan's line algorithm to get the coordinates of the line through the interpolated
%subvolume
[m X1]=min(abs(nx-xi(1)));
[m X2]=min(abs(nx-xi(2)));
[m Y1]=min(abs(ny-yi(1)));
[m Y2]=min(abs(ny-yi(2)));
[m Z1]=min(abs(nz-zi(1)));
[m Z2]=min(abs(nz-zi(2)));
[Xp,Yp,Zp] = bresenham_line3d([X1 Y1 Z1], [X2 Y2 Z2],0);

%Return the positions and pixels values of the line
cx=nx(Xp);
cy=ny(Yp);
cz=nz(Zp);
c = interp3(x,y,z,I,cx,cy,cz,method);
end

function [X,Y,Z] = bresenham_line3d(P1, P2, precision)
    if ~exist('precision','var') | isempty(precision) | round(precision) == 0
      precision = 0;
      P1 = round(P1);
      P2 = round(P2);
   else
      precision = round(precision);
      P1 = round(P1*(10^precision));
      P2 = round(P2*(10^precision));
   end

   d = max(abs(P2-P1)+1);
   X = zeros(1, d);
   Y = zeros(1, d);
   Z = zeros(1, d);

   x1 = P1(1);
   y1 = P1(2);
   z1 = P1(3);

   x2 = P2(1);
   y2 = P2(2);
   z2 = P2(3);

   dx = x2 - x1;
   dy = y2 - y1;
   dz = z2 - z1;

   ax = abs(dx)*2;
   ay = abs(dy)*2;
   az = abs(dz)*2;

   sx = sign(dx);
   sy = sign(dy);
   sz = sign(dz);

   x = x1;
   y = y1;
   z = z1;
   idx = 1;

   if(ax>=max(ay,az))			% x dominant
      yd = ay - ax/2;
      zd = az - ax/2;

      while(1)
         X(idx) = x;
         Y(idx) = y;
         Z(idx) = z;
         idx = idx + 1;

         if(x == x2)		% end
            break;
         end

         if(yd >= 0)		% move along y
            y = y + sy;
            yd = yd - ax;
         end

         if(zd >= 0)		% move along z
            z = z + sz;
            zd = zd - ax;
         end

         x  = x  + sx;		% move along x
         yd = yd + ay;
         zd = zd + az;
      end
   elseif(ay>=max(ax,az))		% y dominant
      xd = ax - ay/2;
      zd = az - ay/2;

      while(1)
         X(idx) = x;
         Y(idx) = y;
         Z(idx) = z;
         idx = idx + 1;

         if(y == y2)		% end
            break;
         end

         if(xd >= 0)		% move along x
            x = x + sx;
            xd = xd - ay;
         end

         if(zd >= 0)		% move along z
            z = z + sz;
            zd = zd - ay;
         end

         y  = y  + sy;		% move along y
         xd = xd + ax;
         zd = zd + az;
      end
   elseif(az>=max(ax,ay))		% z dominant
      xd = ax - az/2;
      yd = ay - az/2;

      while(1)
         X(idx) = x;
         Y(idx) = y;
         Z(idx) = z;
         idx = idx + 1;

         if(z == z2)		% end
            break;
         end

         if(xd >= 0)		% move along x
            x = x + sx;
            xd = xd - az;
         end

         if(yd >= 0)		% move along y
            y = y + sy;
            yd = yd - az;
         end

         z  = z  + sz;		% move along z
         xd = xd + ax;
         yd = yd + ay;
      end
   end

   if precision ~= 0
      X = X/(10^precision);
      Y = Y/(10^precision);
      Z = Z/(10^precision);
   end

   return;					% bresenham_line3d
end


