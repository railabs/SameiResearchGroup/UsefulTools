% Copyright 20xx - 2019. Duke University
function noise=FilteredByNPS(NPS,I,psize,stdNoise)
%This function creates noise as filtered by a given NPS.

%compute the nyquist
ny = 1./(2*psize);
ny = [ny(2) ny(1) ny(3)];

%get the frequenices for each dimension (of the input image)
for i=1:length(ny)
    f{i}=linspace(-ny(i),ny(i),size(I,i));
end
[U,V,W] = meshgrid(f{2},f{1},f{3});

%interpolate to get NPS at frequencies of the image
NPS = interp3(NPS.fx,NPS.fy,NPS.fz,NPS.NPS,U,V,W,'Linear',0);

%make white Guassian noise with mean 0 and std 1
noise=random('norm',0,1,size(I));

%take Fourier transform
noise = (fftn(noise));

%filter noise by NPS
noise=noise.*NPS;
noise=(abs(ifftn(noise)));

% scale noise to have the desired magnitude
noise=noise./std(noise(:));
noise=noise.*stdNoise;

%shift so noise is zero mean
noise = noise-mean(noise(:));
end