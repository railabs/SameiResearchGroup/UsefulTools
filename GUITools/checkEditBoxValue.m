% Copyright 20xx - 2019. Duke University
function new = checkEditBoxValue(hObject,checkType)
%Get the old value
old = get(hObject,'UserData');
%get the new value
new = str2num(get(hObject,'String'));
if isempty(new)
    new = old;
end

switch checkType
    case 'GreaterThanZero'
        if new<=0 || ~isreal(new)
            new = old;
        end
    case 'GreaterThanZeroInt'
        new=round(new);
        if new<0 || ~isreal(new)
            new = old;
        end
    case 'AnyRealValue'
        if ~isreal(new)
            new = old;
        end
end
set(hObject,'String',num2str(new));
set(hObject,'UserData',new);
end