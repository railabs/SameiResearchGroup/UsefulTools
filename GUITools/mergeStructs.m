% Copyright 20xx - 2019. Duke University
function out = mergeStructs(structs)

%Structs is a cell array of structured variable
for i=1:length(structs)
    temp = structs{i};
    names = fieldnames(temp);
    for j=1:length(names)
        out.(names{j})=temp.(names{j});
    end
end


end