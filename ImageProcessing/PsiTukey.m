% Copyright 20xx - 2019. Duke University
function Psi = PsiTukey(x,sigma)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluate the "edge-stopping" function based on Tukey's biweight used in 
% anisotropic diffusion.  
%
% x - Image of intensity difference between pixels and one of its 
% neighborhoods (ie right,left,top,or bottom).
%
% sigma - scale of the gradient magnitudes to preserve
%
% Called by RobustAnisotropicDiffusion
%
% Greg Sturgeon
% Created for BMME 790
% December 3, 2009
% Reference: M.J. Black, G. Sapiro, D. Marimont and D. Heeger. Robust 
% Anisotropic Diffusion.  IEEE Transaction on Image Processing, 7(3):421-
% 432, March 1998.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


[m n] =size(x);

Psi = zeros(size(x));

for i=1:m
    for j=1:n
        if abs(x(i,j))<= sigma
            Psi(i,j) = x(i,j)*(1-(x(i,j)/sigma)^2)^2;
        else
            Psi(i,j) = 0;
        end
    end
end

