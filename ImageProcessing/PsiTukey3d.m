% Copyright 20xx - 2019. Duke University
function Psi = PsiTukey3d(x,sigma)
[m n p] =size(x);

Psi = zeros(size(x));


if length(sigma) > 1
    for k=1:p
        Psi(:,:,k) = x(:,:,k).*(1-(x(:,:,k)/sigma(p)).^2).^2;

        mZero = abs(x(:,:,k))<=sigma(p);
        Psi(:,:,k) = Psi(:,:,k).*mZero;
    end
else
    for k=1:p
        Psi(:,:,k) = x(:,:,k).*(1-(x(:,:,k)/sigma).^2).^2;

        mZero = abs(x(:,:,k))<=sigma;
        Psi(:,:,k) = Psi(:,:,k).*mZero;
    end
end


% for i=1:m
%     for j=1:n
%         for k=1:p
%             if abs(x(i,j,k))<= sigma
%                 Psi(i,j,k) = x(i,j,k)*(1-(x(i,j,k)/sigma)^2)^2;
%             else
%                 Psi(i,j,k) = 0;
%             end
%         end
%     end
% end