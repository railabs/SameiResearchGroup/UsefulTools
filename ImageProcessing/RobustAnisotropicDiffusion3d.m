% Copyright 20xx - 2019. Duke University
function Is_t1 = RobustAnisotropicDiffusion3d(Is,itt)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform robust anisotropic diffusion based on robust estimation to
% provide smoothing with edge preservation.  
% Is - 2d input image
% itt - number of itterations to preform
%
% Greg Sturgeon
% Created for BMME 790
% December 3, 2009
% Reference: M.J. Black, G. Sapiro, D. Marimont and D. Heeger. Robust 
% Anisotropic Diffusion.  IEEE Transaction on Image Processing, 7(3):421-
% 432, March 1998.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[m,n,p] = size(Is);

Is_t1 = zeros(m,n,p);
[Igradx,Igrady,Igradz] = gradient(Is);
Igradmag = (Igradx.^2 + Igrady.^2 + Igradz.^2).^.5;


sigma = 1.4826*mad(reshape(Igradmag,m*n*p,1),1)
if sigma ==0
    sigma = 1.253*mad(reshape(Igradmag,m*n*p,1),0);
end
lambda = 1;
i=1;

while i<=itt
    
    Ipr = Is(1:m-2,2:n-1,2:p-1);
    Ipl = Is(3:m,2:n-1,2:p-1);
    Ipt = Is(2:m-1,1:n-2,2:p-1);
    Ipb = Is(2:m-1,3:n,2:p-1);
    
    Ipfr = Is(2:m-1,2:n-1,1:p-2);
    Ipbk = Is(2:m-1,2:n-1,3:p);
    

    % Evaluate for the center
    Is_t1(2:m-1,2:n-1,2:p-1) = Is(2:m-1,2:n-1,2:p-1) + (lambda./6) *...
        (PsiTukey3d(Ipr-Is(2:m-1,2:n-1,2:p-1),sigma) + ...
        PsiTukey3d(Ipl-Is(2:m-1,2:n-1,2:p-1),sigma) + ...
        PsiTukey3d(Ipt-Is(2:m-1,2:n-1,2:p-1),sigma) + ...
        PsiTukey3d(Ipb-Is(2:m-1,2:n-1,2:p-1),sigma) + ...
        PsiTukey3d(Ipfr-Is(2:m-1,2:n-1,2:p-1),sigma) + ...
        PsiTukey3d(Ipbk-Is(2:m-1,2:n-1,2:p-1),sigma));
      
    
    Is(2:m-1,2:n-1,2:p-1) = Is_t1(2:m-1,2:n-1,2:p-1);
    i=i+1;
end
