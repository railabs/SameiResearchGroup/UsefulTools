% Copyright 20xx - 2019. Duke University
function Is = RobustAnisotropicDiffusion3dSigma(Is,itt,sigma)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform robust anisotropic diffusion based on robust estimation to
% provide smoothing with edge preservation.  
% Is - 2d input image
% itt - number of itterations to preform
%
% Greg Sturgeon
% Created for BMME 790
% December 3, 2009
% Reference: M.J. Black, G. Sapiro, D. Marimont and D. Heeger. Robust 
% Anisotropic Diffusion.  IEEE Transaction on Image Processing, 7(3):421-
% 432, March 1998.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[m n p] = size(Is);

lambda = 1;
i=1;

while i<=itt
    Psi3d = zeros(m-2,n-2,p-2);
    Psi3d = Psi3d + PsiTukey3d(Is(1:m-2,2:n-1,2:p-1)-Is(2:m-1,2:n-1,2:p-1),sigma);
    Psi3d = Psi3d + PsiTukey3d(Is(3:m,2:n-1,2:p-1)-Is(2:m-1,2:n-1,2:p-1),sigma);
    Psi3d = Psi3d + PsiTukey3d(Is(2:m-1,1:n-2,2:p-1)-Is(2:m-1,2:n-1,2:p-1),sigma);
    Psi3d = Psi3d + PsiTukey3d(Is(2:m-1,3:n,2:p-1)-Is(2:m-1,2:n-1,2:p-1),sigma);
    Psi3d = Psi3d + PsiTukey3d(Is(2:m-1,2:n-1,1:p-2)-Is(2:m-1,2:n-1,2:p-1),sigma);
    Psi3d = Psi3d + PsiTukey3d(Is(2:m-1,2:n-1,3:p)-Is(2:m-1,2:n-1,2:p-1),sigma);
    
    
    Is(2:m-1,2:n-1,2:p-1) = Is(2:m-1,2:n-1,2:p-1) + ((lambda./6)*Psi3d);
    i=i+1;
end
