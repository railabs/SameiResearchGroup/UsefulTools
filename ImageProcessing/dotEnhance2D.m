% Copyright 20xx - 2019. Duke University
function im = dotEnhance2D(I,scaleRange,N)
%This function runs a 2D dot enhancing filter on image I. Could be used as
%an intermediary step in detecting lung nodules.
%
%Inputs--------------------------------------------------------------------
%   I           Image to be filtered. Can be 2D or 3D. If I is 3D, each
%               slice will be filtered individually
%   scaleRange  2x1 vector specifying the range of scales (in pixels). This
%               parameter dictates the size of dots that will be enhanced
%               from the filter.
%   N           Number of scales. The image is filtered for each of N
%               scales (within scaleRange). The final filtered image is the
%               maximum accross all scales.
%--------------------------------------------------------------------------
%
%Ouputs--------------------------------------------------------------------
%   im          The dot enhanced image. Will be same size as I
%--------------------------------------------------------------------------

%Written by Justin Solomon
%based on Li, Q., Sone, S., & Doi, K. (2003). Selective enhancement filters
%for nodules, vessels, and airway walls in two- and three-dimensional CT
%scans. Medical Physics, 30(8), 2040?2051. doi:10.1118/1.1581411

%get the range of scales
sig1=scaleRange(1)/4;
r=(scaleRange(1)/scaleRange(2)).^(1/(N-1));
sigs = sig1*r.^(0:N-1);

%Filter each slice individually
im=zeros(size(I));
for i=1:size(I,3)
    im(:,:,i)=dotEnhanceFilter(I(:,:,i),sigs,N);
end


end

function im = dotEnhanceFilter(I,sigs,N)

%initialize im
im=zeros(size(I,1),size(I,2),N);

%loop through each scale and run dot enhance filter for that scale
for i=1:N
    %get the size of the gaussian smoothing filter
    hsize = 2*ceil(2*sigs(i))+1;
    
    %get the filter
    h = fspecial('gaussian',hsize,sigs(i));
    
    %smooth the image
    Ismooth = imfilter(I,h,'symmetric','conv');
    
    %get first and second derivatives
    [fx, fy] = imgradientxy(Ismooth,'CentralDifference');
    [fxx, fxy] = imgradientxy(fx,'CentralDifference');
    [fyx, fyy] = imgradientxy(fy,'CentralDifference');
    
    
    %compute eigenvalues
    K = (fxx+fyy)/2;
    Q = sqrt(fxx.*fyy -fxy.*fyx);
    temp=sqrt(K.^2 - Q.^2);
    lambda1 = K + temp;
    lambda2 = K - temp;
    
    %exchange values of lambda1 and lambda2 when neccessary
    mask = abs(lambda1)<abs(lambda2);
    temp  = lambda1(mask);
    lambda1(mask)=lambda2(mask);
    lambda2(mask)=temp;
    
    %compute the values of the filter
    mask = lambda1>=0 & lambda2 >=0;
    zdot = (abs(lambda2).^2)./abs(lambda1);
    zdot(mask)=0;
    zdot = zdot * (sigs(i).^2);
    im(:,:,i)=zdot;
end

%take max accross all scales
im = max(im,[],3);


end