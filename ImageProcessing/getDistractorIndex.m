% Copyright 20xx - 2019. Duke University
function distractorIndex = getDistractorIndex(I,varargin)
%This function computes the local distractor index for every pixel in the
%3D image, I. The distractor index is defined as the weighted average of
%voxels within a local VOI that are classified as distracting. The
%algorithm classifies voxels as distracting versus non-distrating by
%thresholding a dot/line enhanced version of the image. The enhancing
%filters are meant to highlight image features that are similar to
%potential features of interest (e.g., lung vessels). 
%
%Syntax--------------------------------------------------------------------
%   distractorIndex = getDistractorIndex(I) uses the default settings. I is
%   a 3D image volume
%
%   distractorIndex = getDistractorIndex(I,name,value) lets you set the
%   optional settings using name-value pairs. See below for list of
%   options. ex getDistractorIndex(I,'WL',[W L])
%
%--------------------------------------------------------------------------
%
%Optional settings----------------------------------------------------------
%
%   mask, default = true(size(I))
%   Binary mask of denoting the global region being considered. Only voxels
%   within the mask can be classified as distracting. A common use of this
%   is to segment the lungs. Otherwise, the lung borders would be
%   considered distracting when they are probably not (for the task of
%   nodule detection). If you set mask = [], it will assume all voxels are
%   potentially distracting.
%
%   scaleRange, default = [10 15];
%   2x1 vector specifying the range of scales (in pixels). This parameter
%   dictates the size of lines/dots that will be enhanced from the
%   selective enhancement portion of the code.
%
%   N, default = 5;
%   Number of scales. The image is filtered for each of N scales (within
%   scaleRange). The final filtered image is the maximum accross all
%   scales.
%
%   nGray, default = 256;
%   Number of gray levels to use when applying the window and leveling. The
%   image volume is quantized to integer values before being filtered. This
%   process is controlled by nGray, and WL (see below). This lets you
%   compute a distractor index for an image, as it is displayed to a human
%   reader.
%
%   WL, default = [range(I(:)) mean([min(I(:)) max(I(:))])];
%   A 1x2 vectore with Window and level values used to quantize the image.
%
%   T, default = .3;
%   Threshold value (between 0 to 1) used to binarize the dot/line enhanced
%   image. Increasing T will result in less voxels being classified as
%   distracting, and thus will lower the distractor index.
%
%   Fovsize, default = 25;
%   %radius of the "fovia" (i.e., in-plane ROI size) in pixels used to
%   compute the weighted avearge of distracting pixels. The averaging
%   kernel used is a 3D Gaussian, with inplane STD (in pixels) of Fovsize/2
%   and kernel size of 2*Fovsize+1; The z direction kernel parameters are
%   controled by rSlices (see below). Increasing Fovsize or rSlices is
%   effectively like increasing the ROI size over which to compute the
%   weighted average of distracting voxels.
%
%   rSlices, default = 2;
%   controls the size and STD of the aforementioned 3D Gaussian averaging
%   kernel in the z-direction. The STD is (2*rSlices+1)/4 and the kernel
%   size is 2*rSlices+1.
%
%   filterMode, default = '2D';
%   controls if the enhancing filters are run in '2D' or '3D'. Note that 3D
%   dot/line enhancing filters are significantly slower than 2D and thus
%   are not recommended.
%
%   verbose, default = false;
%   boolean that controls if the status of the algorithm is printed to
%   screen.
%--------------------------------------------------------------------------
%
%Notes---------------------------------------------------------------------
%written by Justin Solomon, requires other functions in the UsefulTools
%repo to run. May also require Matlab 2016 (for the 3D gaussian filtering).
%If thats a problem, it woudn't be too hard to change that part of the code
%Send questions to justin.solomon@duke.edu
%--------------------------------------------------------------------------

%get the settings
[mask, scaleRange, N, nGray, WL, T, Fovsize, rSlices, filterMode, verbose]=parseInputs(varargin,I);

%quantize the image
if verbose; disp(['Quantizing the image to ' num2str(nGray) ' gray levels']); end
W=WL(1); L=WL(2);
I = mat2gray(I,[L-W/2 L+W/2]); %apply window and level
I=gray2ind(I,nGray);    %quantize

%Get enhanced images
if verbose; disp(['Running ' filterMode ' enhancing filters']); end
switch filterMode
    case '2D'
        imdot = dotEnhance2D(I,scaleRange,N);
        imline = lineEnhance2D(I,scaleRange,N);
    case '3D'
        imdot = dotEnhance3D(I,scaleRange,N,'Verbose',verbose);
        imline = lineEnhance3D(I,scaleRange,N,'Verbose',verbose);
end
im = imdot+imline; clear imdot imline;
lims = [min(im(mask)) max(im(mask))]; range = diff(lims);

%get binary mask of distracting voxels
if verbose; disp(['Thresholding dot/line enhanced image, T =  ' sprintf('%3.2f',T)]); end
mask_distractors = im > lims(1) + T*range & mask;

%Get the distractor index
if verbose; disp('3D Gaussian averaging'); end
distractorIndex = imgaussfilt3(double(mask_distractors),[Fovsize/2 Fovsize/2 (2*rSlices+1)/4],'FilterSize',[2*Fovsize+1 2*Fovsize+1 2*rSlices+1]);

if verbose; disp('Done!'); end

end



function [mask, scaleRange, N, nGray, WL, T, Fovsize, rSlices, filterMode, verbose]=parseInputs(args,I)
%defaults
mask=true(size(I));
scaleRange=[10 15];
N=5;
nGray = 256;
WL = [range(I(:)) mean([min(I(:)) max(I(:))])];
T = .3;
Fovsize = 25;
rSlices = 2;
filterMode = '2D';
verbose = true;

if ~isempty(args)
    for i=1:2:length(args)
        switch args{i}
            case 'mask'
                mask=args{i+1};
            case 'scaleRange'
                scaleRange=args{i+1};
            case 'N'
                N=args{i+1};
            case 'WL'
                WL=args{i+1};
            case 'T'
                T=args{i+1};
            case 'Fovsize'
                Fovsize=args{i+1};
            case 'rSlices'
                rSlices=args{i+1};
            case 'filterMode'
                filterMode=args{i+1};
            case 'Verbose'
                verbose = args{i+1};  
        end
    end
    
end
end