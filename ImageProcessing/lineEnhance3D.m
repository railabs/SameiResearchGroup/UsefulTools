% Copyright 20xx - 2019. Duke University
function im = lineEnhance3D(I,scaleRange,N,varargin)
%This function runs a 3D line enhancing filter on image I. Could be used as
%an intermediary step in detecting lung nodules.
%
%Inputs--------------------------------------------------------------------
%   I           Image to be filtered.
%   scaleRange  2x1 vector specifying the range of scales (in pixels). This
%               parameter dictates the size of dots that will be enhanced
%               from the filter.
%   N           Number of scales. The image is filtered for each of N
%               scales (within scaleRange). The final filtered image is the
%               maximum accross all scales.
%--------------------------------------------------------------------------
%
%Ouputs--------------------------------------------------------------------
%   im          The line enhanced image. Will be same size as I
%--------------------------------------------------------------------------

%Written by Justin Solomon
%based on Li, Q., Sone, S., & Doi, K. (2003). Selective enhancement filters
%for nodules, vessels, and airway walls in two- and three-dimensional CT
%scans. Medical Physics, 30(8), 2040?2051. doi:10.1118/1.1581411

[verbose]=parseInputs(varargin);

%get the range of scales
sig1=scaleRange(1)/4;
r=(scaleRange(1)/scaleRange(2)).^(1/(N-1));
sigs = sig1*r.^(0:N-1);

%get filter response for each scale
im=zeros(size(I));
for i=1:N
    %Get the filter response for the given scale
    temp = dotEnhanceFilter(I,sigs(i),i,N,verbose);
    
    %only save values that are greater than previous iteration (result is
    %that the max accross scales is the final filtered value
    mask = temp>im;
    im(mask)=temp(mask);
end


end

function im = dotEnhanceFilter(I,sig,nScale,N,verbose)

%initialize the output
im = zeros(size(I));

%get the filter size for this scale
hsize = ceil(4*sig);
if ~mod(hsize,2)
    hsize=hsize+1;
end

%smooth the image
I = imgaussfilt3(I,sig,'FilterSize',hsize,'Padding','symmetric');

%get a vector of col indices
cols = 1:size(I,1)*size(I,2);
cols = repmat(cols,[3,1]);
%cols=cols(:);


%filter each slice
for i=1:size(I,3)
    if verbose; disp(['Processing scale ' num2str(nScale) '/' num2str(N) ', Slice ' num2str(i) '/' num2str(size(I,3))]); end
    %Get the local slices of interest (need to go �2 slices because we're
    %taking second derivatives). This requires special cases for the
    %beginning 2 and last 2 slices
    if i>2 && i<size(I,3)-1
        slice = I(:,:,i-2:i+2); %slice contains the slice of interest and surrounding slices (need to be included so 2nd derivative calculations are correct
        ind = 3;
    elseif i<=2
        slice = I(:,:,1:i+2);
        ind =i;
    elseif i>=size(I,3)-1
        slice = I(:,:,i-2:size(I,3));
        ind = 3;
    end
    
    %get first and second derivatives
    [fx, fy, fz]= imgradientxyz(slice,'central' );
    [fxx, fxy, fxz]= imgradientxyz(fx,'central' );
    [fyx, fyy, fyz]= imgradientxyz(fy,'central' );
    [fzx, fzy, fzz]= imgradientxyz(fz,'central' );
    clear fx fy fz;
    
    %extract only the slice of interest;
    fxx=fxx(:,:,ind); fxy=fxy(:,:,ind); fxz=fxz(:,:,ind);
    fyx=fyx(:,:,ind); fyy=fyy(:,:,ind); fyz=fyz(:,:,ind);
    fzx=fzx(:,:,ind); fzy=fzy(:,:,ind); fzz=fzz(:,:,ind);
    
    
    
    %construct stack of hessian matrices
    fun = @(x) permute(x(:),[3 2 1]);
    fxx=fun(fxx); fxy=fun(fxy); fxz=fun(fxz);
    fyx=fun(fyx); fyy=fun(fyy); fyz=fun(fyz);
    fzx=fun(fzx); fzy=fun(fzy); fzz=fun(fzz);
    H = [fxx fxy fxz;...
         fyx fyy fyz;...
         fzx fzy fzz];
     clear fxx fxy fxz fyx fyy fyz fzx fzy fzz
     
     %Get the eigen values (must be estimated numericaly. Uses a fast
     %CardonRoots numerical approximation for this).
     eigs = eig3(H);  %eigs is a 3xNpixels matrix of eigen values for each pixel's Hessian matrix
     mask = eigs>=0;   %find values that are greater than zero (these will be zeroed out)
     [eigs, inds] = sort(abs(eigs),1,'descend'); %sort the eigen values in descending order
     mask = mask(sub2ind(size(eigs),inds,cols)); %resort the mask to match the sorted eigen values
     mask = mask(1:2,:); %The line is conditional only on the first two eigen values being less than zero. so we don't worry about the last value for this mask (i.e., eigs(3,:) doesn't matter for this conditional)
     mask = any(mask,1);
     
     %compute the filter value
     vals = (sig^2)*(eigs(2,:).* ( eigs(2,:) - eigs(3,:)   )./ eigs(1,:)); 
     vals(mask)=0; %mask out pixels where either of the first two eigen values (eigs(1:2,:)) were greater than zero
     im(:,:,i) = reshape(vals,[size(I,1) size(I,2)]);
end

end

function [verbose]=parseInputs(args)
%defaults
verbose = true;
if ~isempty(args)
    for i=1:2:length(args)
        switch args{i}
            case 'Verbose'
                verbose = args{i+1};  
        end
    end
    
end
end