% Copyright 20xx - 2019. Duke University
function C = localComplexity(mask_distractors,mask_ROI,pos,dmax)

%Crop the image volume to a bounding box of the ROI mask
[Y,X,Z]=ind2sub(size(mask_ROI),find(mask_ROI));
xmin=min(X); xmax=max(X);
ymin=min(Y); ymax=max(Y);
zmin=min(Z); zmax=max(Z);
mask_distractors=mask_distractors(ymin:ymax,xmin:xmax,zmin:zmax);
mask_ROI=mask_ROI(ymin:ymax,xmin:xmax,zmin:zmax);

%get the in plane position
x=pos(1)-xmin+1; y=pos(2)-ymin+1;

%loop over slices
C = zeros(size(mask_ROI,3),1);
for i=1:size(mask_ROI,3)
    slice = mask_distractors(:,:,i) & mask_ROI(:,:,i);
    CC = bwconncomp(slice);
    Nparts = CC.NumObjects;
    if Nparts
        %initialize stats that will be recorded for each particle
        Dw=zeros(Nparts,1); %weighting according to distance
        %loop over each particle
        for j=1:Nparts;
            %make a mask of just this particle
            temp = false(size(slice));
            temp(CC.PixelIdxList{j})=true;
            
            %compute the distance from the point of interest and the nodule
            D=bwdist(temp);
            
            %convert to a weight
            Dw(j) = 1-D(y,x)/dmax;
        end
        %Compute complexity for each slice
        C(i)=sum(Dw);
    else
        C(i)=0;
    end
    
end

%Take average complexity over slices
C=mean(C);

end