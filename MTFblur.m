% Copyright 20xx - 2019. Duke University
function Iblur = MTFblur(I,MTF,psize)
%This function takes an image and blurs it with a given 3D MTF (input as a
%structured variable)

%compute the nyquist
ny = 1./(2*psize);
ny = [ny(2) ny(1) ny(3)];

%get the frequenices for each dimension
for i=1:length(ny)
    f{i}=linspace(-ny(i),ny(i),size(I,i));
end
[U,V,W] = meshgrid(f{2},f{1},f{3});

%Interpolate the MTF in all 3 directions
Mu = interp1(MTF.fxy,MTF.MTFxy,abs(U),'linear',0);
Mv = interp1(MTF.fxy,MTF.MTFxy,abs(V),'linear',0);
Mw = interp1(MTF.fz,MTF.MTFz,abs(W),'linear',0);

%Compute the full MTF as the product of each direction MTF
MTFint = Mu.*Mv.*Mw;

%Blur the lesion (in Fourier Domain)
Iblur=abs(ifftn(MTFint.*fftshift(fftn(I-min(I(:)))) ) );

Iblur = Iblur+min(I(:));
end