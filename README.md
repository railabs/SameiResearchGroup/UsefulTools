# UsefulTools
This repository is a collection of miscellaneous functions that are generally useful for many projects. For example, if you need to read in a series of CT images, you can save yourself time and just use the readCTSeries.m function. Most of the functions are well commented and described. Many other RAI labs resositories depend on this one (but none of the functions in this repository depend on others...hopefully).
# Authors
Justin Solomon created and manages the respository. Many other lab memeberss have contrubuted to it as well.
