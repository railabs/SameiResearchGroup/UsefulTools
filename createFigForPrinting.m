% Copyright 20xx - 2019. Duke University
function fig = createFigForPrinting(varargin)

switch nargin
    case 0
        fig = figure;
        set(fig,'Units','inches','Visible','off');
        pos = get(fig,'Position');
        xSize = pos(3);
        ySize = pos(4);
        delete(fig);
    case 1
        xSize = varargin{1};
        ySize = xSize;
    case 2
        xSize = varargin{1};
        ySize = varargin{2};
end

fig=figure;
set(fig,'units','inches','position',[0 0 xSize ySize],'PaperUnits','inches')
set(fig,'PaperSize',[xSize ySize])
set(fig,'PaperPosition',[0 0 xSize ySize])
set(fig,'Color','w')
set(fig,'InvertHardCopy','off')

end