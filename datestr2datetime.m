% Copyright 20xx - 2019. Duke University
function t = datestr2datetime(dates)
%This function converts dates that are saved as strings in the form of
%yyyymmdd into datetime matlab objects
% dates is a cell array of date strings
% t is an array of datetime objects

% convert date to matlab datetime objects
if iscell(dates)
    fun = @(str) str2num(str{1});
    try
        t = arrayfun(fun,dates);
        t = datetime(t,'ConvertFrom','yyyymmdd');
    catch
        ind = strcmp(dates,'');
        dates(ind)  = repmat({'19700101'},[sum(ind) 1]);
        fun = @(str) str2num(str{1});
        t = arrayfun(fun,dates);
        t = datetime(t,'ConvertFrom','yyyymmdd');
        t(ind) = NaT;
    end
elseif isnumeric(dates)
    t = datetime(dates,'ConvertFrom','yyyymmdd');
elseif isdatetime(dates)
    t = dates;
elseif ischar(dates)
    dates = str2num(dates);
    t = datetime(dates,'ConvertFrom','yyyymmdd');
    
else
    error([class(dates) ' not supported in datestr2datetime function'])
    
end

end