% Copyright 20xx - 2019. Duke University
function exportIPIPEVolume(output,I,info)
%This function exports .ipv image volume files (part of Siemen's ipipe
%framework).
%
%exportIPIPEVolume(output,I,info) saves the volumetric image data, I, into
%a file called output, with metadata info. The info metadata must come from
%the importIPIPEVolume() function. 

%create the blank file
id = fopen(output,'w');

%write out the image data
I = I-info.rescale;
I=permute(I,[2 1 3]);
fwrite(id,I,info.type);

%get the position of the beginning of the xml footer (will need after
%writing out the xml text
footerPosition = ftell(id);

%write out the xml text
T = xmlwrite(info.xml);
fprintf(id,'%s',T);

%write out the footerPosition number
fwrite(id,footerPosition,'int64');

%write out the end of file tag
fprintf(id,'%s','JsVoxelVolume');

%close the file
fclose(id);
end

