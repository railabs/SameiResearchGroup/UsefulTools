% Copyright 20xx - 2019. Duke University
function [cx, cy, cz]=findSignalLocationSphere(im,rad,x,y,z,ROI,pos,scale)
%This function finds the center location of a shpere in an image using
%the cross-correlation method

%Crop the image
xmin=pos(1)-ROI; xmax=pos(1)+ROI;
ymin=pos(2)-ROI; ymax=pos(2)+ROI;
zmin=pos(3)-ROI; zmax=pos(3)+ROI;
[~,ixmin]=min(abs(xmin-x)); [~,ixmax]=min(abs(xmax-x));
[~,iymin]=min(abs(ymin-y)); [~,iymax]=min(abs(ymax-y));
[~,izmin]=min(abs(zmin-z)); [~,izmax]=min(abs(zmax-z));
im=im(iymin:iymax,ixmin:ixmax,izmin:izmax);
x=x(ixmin:ixmax);
y=y(iymin:iymax);
z=z(izmin:izmax);
[X,Y,Z]=meshgrid(x,y,z);

%Subtract mean from image
im = im-mean(im(:));

%Upsample the image
xq=linspace(x(1),x(end),length(x)*scale);
yq=linspace(y(1),y(end),length(y)*scale);
zq=linspace(z(1),z(end),length(z)*scale);
[Xq,Yq,Zq]=meshgrid(xq,yq,zq);
im = interp3(X,Y,Z,im,Xq,Yq,Zq,'linear');

%Create a sphere
cx=x(1)+(x(end)-x(1))/2;
cy=y(1)+(y(end)-y(1))/2;
cz=z(1)+(z(end)-z(1))/2;
[~,~,R]= cart2sph(Xq-cx,Yq-cy,Zq-cz);
mask = double(R<=rad);

%perform cross correlation
im = imfilter(im,mask,'corr');

%Get the maximum coordinates
[~, imax] = max(im(:));
[iy,ix,iz]=ind2sub(size(im),imax);
cx=xq(ix);
cy=yq(iy);
cz=zq(iz);

end