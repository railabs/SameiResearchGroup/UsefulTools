% Copyright 20xx - 2019. Duke University
function [plane,coeffs] = fitToPlane(pos)

M = [pos(:,1:2) ones(size(pos,1),1)];
z = pos(:,3);
b = inv(M'*M)*(M'*z);
coeffs = [b(1) b(2) -1 b(3)];
plane = @(x,y) -((coeffs(1)*x+coeffs(2)*y+coeffs(4))./coeffs(3));



end