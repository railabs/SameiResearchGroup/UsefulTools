% Copyright 20xx - 2019. Duke University
function [x,y,z]=getCTcoordinates(info)

psize=info(1).PixelSpacing;
x=(0:double(info(1).Width)-1)'; x=x*psize(1); x=x+info(1).ImagePositionPatient(1);
y=(0:double(info(1).Height)-1)'; y=y*psize(2); y=y+info(1).ImagePositionPatient(2);
clear z;
for j=1:length(info)
    z(j,1)=info(j).ImagePositionPatient(3);
end

end