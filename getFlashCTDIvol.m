% Copyright 20xx - 2019. Duke University
function [CTDIvol, varargout] = getFlashCTDIvol(MA,s,config,KVP,pitch,filter,phantom)
%[CTDIvol] = getFlashCTDIvol(MA,s,config,KVP,pitch,filter,phantom)
%[CTDIvol, CTDIw] = getFlashCTDIvol(MA,s,config,KVP,pitch,filter,phantom)
%[CTDIvol, CTDIw, CTDI100_central, CTDI100_peripheral] = getFlashCTDIvol(MA,s,config,KVP,pitch,filter,phantom)
%Configs can be any of {'2x1','6x0.6','16x0.6','1x5','20x0.6','1x10','12x1.2','40x0.6','64x0.6','32x1.2','128x0.6'}
%KVP can be any of {'70','80','100','120','140','Sn 140'}, can be input as number or string. Function will convert to string either way
%filter can be any of {'Standard','Narrow','Wide'}
%phantom can be any of {'16 cm','32 cm'}
%Data is not provided by Siemens for all combinations of kVp, filter, and phantom. Function returns NaN when there is no data available.

%Get the lookup table of CTDI values
[tab, positions, KVPs, filters, phantoms, configs] = getLookupTable; %Tab is a [position X KVP X filter X phantom X config] array of mGy (CTDI100) per mAs. 

%Get the config index
if strcmp(config,'32x0.6')
    config = '64x0.6';
end
iconfig = find(strcmp(config,configs));
if isempty([iconfig])
    warning([config ' configuration not found, using ' configs{11}]);
    iconfig=11;
end

%convert kVp to string if needed
if ~isa(KVP,'char')
    KVP = num2str(KVP);
end
%get the KVP index
iKVP = find(strcmp(KVP,KVPs));
if isempty([iKVP])
    warning([KVP ' kVp not found, using ' KVPs{4}]);
    iKVP=4;
end

%get the filter index
ifilter = find(strcmp(filter,filters));
if isempty([ifilter])
    warning([filter ' bowtie filter not found, using ' filters{1}]);
    ifilter=1;
end

%get the phantom index
iphantom = find(strcmp(phantom,phantoms));
if isempty([iphantom])
    warning([phantom ' CTDI phantom not found, using ' phantoms{2}]);
    iphantom=2;
end

%Get the central/peripheral indices
iC = find(strcmp(positions,'Central'));
iP = find(strcmp(positions,'Peripheral'));

%compute mAs
mAs = MA*s;

%get central CTDI100
D_c = mAs * tab(iC,iKVP,ifilter,iphantom,iconfig);
%get peripheral CTDI100
D_p = mAs * tab(iP,iKVP,ifilter,iphantom,iconfig);

%Get CTDIw
CTDIw = (1/3)*D_c+ (2/3)*D_p;

%Get CTDIvol
CTDIvol = CTDIw/pitch;

switch nargout-1
    case 1
        varargout{1} = CTDIw;
    case 3
        varargout{1} = CTDIw;
        varargout{2} = D_c;
        varargout{3} = D_p;
end

end

function [tab, positions, KVPs, filters, phantoms, configs] = getLookupTable

%List of all possible collimation configurations/modes
positions={'Central','Peripheral'};
KVPs = {'70','80','100','120','140','Sn 140'};
filters = {'Standard','Narrow','Wide'};
phantoms = {'16 cm','32 cm'};
configs = {'2x1','6x0.6','16x0.6','1x5','20x0.6','1x10','12x1.2','40x0.6','64x0.6','32x1.2','128x0.6'}; %64x0.6 is same as 32x0.6

tab = [ nan nan nan nan nan nan nan nan nan nan nan;...                     standard filter, 16 cm phantom
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        
        nan nan nan nan nan nan nan nan nan nan nan;...                     narrow filter, 16 cm phantom
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        nan nan nan nan nan nan nan nan nan nan nan;...
        
        2.9 4.7 5.0 2.6 3.4 2.6 3.3 3.5 3.3 2.9 2.8;...                     70 kV, central, wide filter, 16 cm phantom
        3.5 5.6 6.0 3.1 4.1 3.1 4.0 4.2 3.9 3.5 3.4;...                     70 kV, peripheral
        4.6 7.4 7.9 4.1 5.4 4.1 5.3 5.6 5.2 4.6 4.5;...                     80 kV, central
        5.3 8.5 9.0 4.7 6.3 4.7 6.1 6.4 5.9 5.3 5.2;...                     80 kV, peripheral
        9.2 14.9 15.8 8.2 10.9 8.2 10.6 11.2 10.4 9.2 9.1;...               100 kV, central
        10.1 16.3 17.3 9.1 12.0 9.1 11.6 12.2 11.4 10.1 9.9;...             100 kV, peripheral
        14.8 23.9 25.4 13.3 17.6 13.3 17.0 17.9 16.7 14.8 14.6;...          120 kV, central
        15.9 25.7 27.2 14.2 18.8 14.2 18.2 19.2 17.9 15.9 15.6;...          120 kV, peripheral
        22.0 35.6 37.8 19.7 26.2 19.7 25.3 26.7 24.9 22.1 21.8;...          140 kV, central
        23.4 37.8 40.0 21.0 27.8 21.0 26.9 28.3 26.4 23.5 23.0;...          140 kV, peripheral
        6.6 10.6 11.3 5.9 7.8 5.9 7.5 8.0 7.4 6.6 6.5;...                   Sn 140 kV, central                                   <--- Tin filter can only be used on Tube B
        6.6 10.7 11.4 6.0 7.9 6.0 7.6 8.0 7.5 6.7 6.5;...                   Sn 140 kV, peripheral
        
        0.6 1.0 1.1 0.6 0.8 0.6 0.7 0.8 0.7 0.6 0.6;...                     70 kV, central, standard filter, 32 cm phantom       <--- This bowtie filter is only for Tube A, which is why there's nans for Sn 140 kV
        1.6 2.6 2.7 1.4 1.9 1.4 1.8 1.9 1.8 1.6 1.6;...                     70 kV, peripheral
        1.1 1.8 1.9 1.0 1.3 1.0 1.3 1.3 1.2 1.1 1.1;...                     80 kV, central
        2.5 4.0 4.2 2.2 2.9 2.2 2.8 2.9 2.7 2.5 2.4;...                     80 kV, peripheral
        2.5 4.1 4.3 2.3 3.0 2.3 2.9 3.1 2.9 2.5 2.5;...                     100 kV, central
        5.0 8.1 8.6 4.5 5.9 4.5 5.8 6.0 5.6 5.0 4.9;...                     100 kV, peripheral
        4.4 7.1 7.5 3.9 5.2 3.9 5.0 5.3 4.9 4.4 4.3;...                     120 kV, central
        8.1 13.2 13.9 7.3 9.6 7.3 9.4 9.8 9.1 8.2 8.0;...                   120 kV, peripheral
        6.5 10.6 11.2 5.9 7.8 5.9 7.5 7.9 7.4 6.6 6.5;...                   140 kV, central
        11.7 19.0 20.0 10.5 13.9 10.5 13.5 14.1 13.2 11.8 11.5;...          140 kV, peripheral
        nan nan nan nan nan nan nan nan nan nan nan;...                     Sn 140 kV, central
        nan nan nan nan nan nan nan nan nan nan nan;...                     Sn 140 kV, peripheral
        
        0.6 0.9 1.0 0.5 0.7 0.5 0.6 0.7 0.6 0.6 0.6;...                     70 kV, central, narrow filter, 32 cm phantom
        1.2 2.0 2.1 1.1 1.4 1.1 1.4 1.5 1.4 1.2 1.2;...                     70 kV, peripheral
        1.0 1.6 1.7 0.9 1.2 0.9 1.1 1.2 1.1 1.0 1.0;...                     80 kV, central
        1.9 3.1 3.3 1.7 2.3 1.7 2.2 2.3 2.1 1.9 1.9;...                     80 kV, peripheral
        2.2 3.6 3.8 2.0 2.6 2.0 2.6 2.7 2.5 2.2 2.2;...                     100 kV, central
        4.0 6.4 6.7 3.6 4.7 3.6 4.6 4.8 4.4 4.0 3.9;...                     100 kV, peripheral
        3.9 6.3 6.7 3.5 4.6 3.5 4.5 4.7 4.4 3.9 3.8;...                     120 kV, central
        6.5 10.6 11.1 5.9 7.7 5.9 7.5 7.8 7.3 6.6 6.4;...                   120 kV, peripheral
        6.1 9.9 10.5 5.5 7.3 5.5 7.1 7.4 6.9 6.2 6.0;...                    140 kV, central
        10.0 16.1 16.9 8.9 11.7 8.9 11.5 12.0 11.2 10.0 9.7;...             140 kV, peripheral
        2.2 3.6 3.8 2.0 2.6 2.0 2.5 2.7 2.5 2.2 2.2;...                     Sn 140 kV, central
        3.2 5.2 5.4 2.9 3.8 2.9 3.7 3.8 3.6 3.2 3.1;...                     Sn 140 kV, peripheral                               <--- Tin filter can only be used on Tube B
        
        0.7 1.2 1.2 0.6 0.9 0.6 0.8 0.9 0.8 0.7 0.7;...                     70 kV, central, wide filter, 32 cm phantom
        2.2 3.5 3.7 2.0 2.6 2.0 2.5 2.6 2.5 2.2 2.2;...                     70 kV, peripheral
        1.2 2.0 2.1 1.1 1.5 1.1 1.4 1.5 1.4 1.2 1.2;...                     80 kV, central
        3.3 5.4 5.7 3.0 3.9 3.0 3.8 4.0 3.7 3.3 3.3;...                     80 kV, peripheral
        2.7 4.4 4.7 2.5 3.3 2.5 3.2 3.3 3.1 2.8 2.7;...                     100 kV, central
        6.4 10.4 10.9 5.7 7.6 5.7 7.4 7.7 7.2 6.4 6.3;...                   100 kV, peripheral
        4.7 7.6 8.0 4.2 5.6 4.2 5.4 5.7 5.3 4.7 4.6;...                     120 kV, central
        10.1 16.4 17.3 9.1 12.0 9.1 11.6 12.2 11.4 10.2 9.9;...             120 kV, peripheral
        7.3 11.8 12.6 6.6 8.7 6.6 8.4 8.9 8.3 7.3 7.2;...                   140 kV, central
        15.0 24.2 25.6 13.4 17.7 13.4 17.2 18.1 16.8 15.0 14.7;...          140 kV, peripheral
        2.5 4.1 4.3 2.3 3.0 2.3 2.9 3.1 2.8 2.5 2.5;...                     Sn 140 kV, central
        4.3 6.9 7.3 3.8 5.1 3.8 4.9 5.2 4.8 4.3 4.2;...                     Sn 140 kV, peripheral
        
        ];
        
    
tab = reshape(tab,[length(positions) length(KVPs) length(filters) length(phantoms) length(configs)]);

%Scale the table to be per mAs
tab = tab/100;

end