% Copyright 20xx - 2019. Duke University
function [CTDIvol, varargout] = getForceCTDIvol(MA,s,config,KVP,pitch,filter,phantom)
%[CTDIvol] = getForceCTDIvol(MA,s,config,KVP,pitch,filter,phantom)
%[CTDIvol, CTDIw] = getForceCTDIvol(MA,s,config,KVP,pitch,filter,phantom)
%[CTDIvol, CTDIw, CTDI100_central, CTDI100_peripheral] = getForceCTDIvol(MA,s,config,KVP,pitch,filter,phantom)
%Configs can be any of {'2x1','6x0.6','1x5','1x10','12x1.2','32x0.6 UHR','64x0.6 sUHR','64x0.6','96x0.6','128x0.6','192x0.6','24x1.2','48x1.2'}
%KVP can be any of {'70','80','90','100','110','120','130','140','150','Sn 100','Sn 150'}, can be input as number or string. Function will convert to string either way
%filter can be any of {'Standard','Narrow'}
%phantom can be any of {'16 cm','32 cm'}

%Get the lookup table of CTDI values
[tab, positions, KVPs, filters, phantoms, configs] = getLookupTable; %Tab is a [position X KVP X filter X phantom X config] array of mGy (CTDI100) per mAs. 

%Get the config index
iconfig = find(strcmp(config,configs));
if isempty([iconfig])
    warning([config ' configuration not found, using ' configs{10}]);
    iconfig=10;
end 

%convert kVp to string if needed
if ~isa(KVP,'char')
    KVP = num2str(KVP);
end
%get the KVP index
iKVP = find(strcmp(KVP,KVPs));
if isempty([iKVP])
    warning([KVP ' kVp not found, using ' KVPs{6}]);
    iKVP=6;
end

%get the filter index
ifilter = find(strcmp(filter,filters));
if isempty([ifilter])
    warning([filter ' bowtie filter not found, using ' filters{1}]);
    ifilter=1;
end

%get the phantom index
iphantom = find(strcmp(phantom,phantoms));
if isempty([iphantom])
    warning([phantom ' CTDI phantom not found, using ' phantoms{2}]);
    iphantom=2;
end

%Get the central/peripheral indices
iC = find(strcmp(positions,'Central'));
iP = find(strcmp(positions,'Peripheral'));

%compute mAs
mAs = MA*s;

%get central CTDI100
D_c = mAs * tab(iC,iKVP,ifilter,iphantom,iconfig);
%get peripheral CTDI100
D_p = mAs * tab(iP,iKVP,ifilter,iphantom,iconfig);

%Get CTDIw
CTDIw = (1/3)*D_c+ (2/3)*D_p;

%Get CTDIvol
CTDIvol = CTDIw/pitch;

switch nargout-1
    case 1
        varargout{1} = CTDIw;
    case 3
        varargout{1} = CTDIw;
        varargout{2} = D_c;
        varargout{3} = D_p;
end

end

function [tab, positions, KVPs, filters, phantoms, configs] = getLookupTable

%List of all possible collimation configurations/modes
positions={'Central','Peripheral'};
KVPs = {'70','80','90','100','110','120','130','140','150','Sn 100','Sn 150'};
filters = {'Standard','Narrow'};
phantoms = {'16 cm','32 cm'};
configs = {'2x1','6x0.6','1x5','1x10','12x1.2','32x0.6 UHR','64x0.6 sUHR','64x0.6','96x0.6','128x0.6','192x0.6','24x1.2','48x1.2'};

%Construct the table, units are mGy (CTDI100) per 100 mAs
%These values come from the Force Technical manual- "Dosimetry and imaging performance report"
tab  = [2.53 4.21 2.44 2.44 3.48 2.75 2.96 3.36 3.05 2.90 2.72 3.00 2.73;...                70 kV, central, standard filter, 16 cm phantom
        2.72 4.53 2.63 2.63 3.75 2.96 3.18 3.62 3.28 3.12 2.93 3.23 2.93;...                70 kV, peripheral
        4.04 6.73 3.90 3.90 5.56 4.40 4.72 5.37 4.87 4.62 4.35 4.79 4.35;...                80 kV, central
        4.24 7.07 4.10 4.10 5.85 4.63 4.97 5.65 5.12 4.86 4.58 5.03 4.58;...                80 kV, peripheral
        5.93 9.88 5.73 5.73 8.17 6.46 6.94 7.89 7.16 6.79 6.39 7.03 6.40;...                90 kV, central
        6.15 10.25 5.94 5.94 8.47 6.70 7.19 8.18 7.42 7.04 6.63 7.29 6.63;...               90 kV, peripheral
        8.08 13.47 7.81 7.81 11.14 8.81 9.45 10.75 9.76 9.26 8.71 9.58 8.72;...             100 kV, central
        8.31 13.84 8.02 8.02 11.45 9.05 9.72 11.05 10.03 9.52 8.95 9.85 8.96;...            100 kV, peripheral
        10.45 17.42 10.10 10.10 14.41 11.39 12.23 13.91 12.62 11.98 11.27 12.40 11.28;...   110 kV, central
        10.69 17.81 10.33 10.33 14.73 11.65 12.50 14.22 12.90 12.25 11.52 12.68 11.53;...   110 kV, peripheral
        13.06 21.76 12.62 12.62 18.00 14.23 15.28 17.37 15.76 14.96 14.07 15.49 14.09;...   120 kV, central
        13.30 22.17 12.85 12.85 18.34 14.50 15.56 17.70 16.06 15.24 14.34 15.78 14.35;...   120 kV, peripheral
        15.73 26.21 15.20 15.20 21.68 17.14 18.40 20.92 18.99 18.02 16.95 18.65 16.97;...   130 kV, central
        16.01 26.68 15.47 15.47 22.06 17.45 18.73 21.29 19.33 18.34 17.26 18.98 17.27;...   130 kV, peripheral
        18.55 30.92 17.93 17.93 25.57 20.22 21.70 24.68 22.40 21.26 20.00 22.00 20.01;...   140 kV, central
        18.85 31.42 18.21 18.21 25.98 20.54 22.05 25.07 22.76 21.60 20.32 22.35 20.33;...   140 kV, peripheral
        21.49 35.82 20.77 20.77 29.62 23.42 25.15 28.59 25.95 24.63 23.17 25.49 23.19;...   150 kV, central
        21.85 36.42 21.11 21.11 30.11 23.81 25.56 29.06 26.38 25.04 23.55 25.91 23.57;...   150 kV, peripheral
        0.79 1.31 0.76 0.76 1.08 0.86 0.92 1.05 0.95 0.90 0.85 0.93 0.85;...                Sn 100 kV, central
        0.77 1.28 0.74 0.74 1.06 0.84 0.90 1.02 0.93 0.88 0.83 0.91 0.83;...                Sn 100 kV, peripheral
        4.99 8.31 4.82 4.82 6.87 5.43 5.83 6.63 6.02 5.71 5.38 5.91 5.38;...                Sn 150 kV, central
        5.02 8.36 4.85 4.85 6.92 5.47 5.87 6.67 6.06 5.75 5.41 5.95 5.41;...                Sn 150 kV, peripheral
        
        2.28 3.80 2.20 2.20 3.14 2.48 2.67 3.03 2.75 2.61 2.46 2.70 2.46;...                70 kV, central, narrow filter, 16 cm phantom
        2.24 3.73 2.16 2.16 3.09 2.44 2.62 2.98 2.71 2.57 2.42 2.66 2.42;...                70 kV, peripheral
        3.67 6.12 3.55 3.55 5.06 4.00 4.29 4.88 4.43 4.21 3.96 4.35 3.96;...                80 kV, central
        3.56 5.93 3.44 3.44 4.91 3.88 4.16 4.74 4.30 4.08 3.84 4.22 3.84;...                80 kV, peripheral
        5.43 9.05 5.25 5.25 7.49 5.92 6.35 7.23 6.56 6.22 5.86 6.44 5.86;...                90 kV, central
        5.22 8.70 5.04 5.04 7.19 5.69 6.10 6.94 6.30 5.98 5.62 6.19 5.63;...                90 kV, peripheral
        7.45 12.41 7.20 7.20 10.26 8.12 8.71 9.91 8.99 8.53 8.03 8.83 8.03;...              100 kV, central
        7.11 11.86 6.87 6.87 9.81 7.75 8.32 9.46 8.59 8.15 7.67 8.44 7.67;...               100 kV, peripheral
        9.66 16.11 9.34 9.34 13.32 10.53 11.31 12.86 11.67 11.07 10.42 11.46 10.43;...      110 kV, central
        9.23 15.38 8.92 8.92 12.72 10.06 10.80 12.28 11.14 10.57 9.95 10.94 9.96;...        110 kV, peripheral
        12.10 20.16 11.69 11.69 16.67 13.18 14.15 16.09 14.60 13.86 13.04 14.34 13.05;...   120 kV, central
        11.56 19.27 11.17 11.17 15.94 12.60 13.53 15.38 13.96 13.25 12.46 13.71 12.47;...   120 kV, peripheral
        14.63 24.38 14.13 14.13 20.16 15.94 17.12 19.46 17.66 16.76 15.77 17.35 15.78;...   130 kV, central
        13.97 23.29 13.50 13.50 19.26 15.23 16.35 18.59 16.87 16.01 15.06 16.57 15.08;...   130 kV, peripheral
        17.30 28.83 16.71 16.71 23.84 18.85 20.24 23.01 20.88 19.82 18.64 20.51 18.66;...   140 kV, central
        16.54 27.56 15.98 15.98 22.79 18.02 19.35 22.00 19.97 18.95 17.83 19.61 17.84;...   140 kV, peripheral
        20.10 33.49 19.42 19.42 27.70 21.90 23.51 26.73 24.26 23.03 21.66 23.83 21.68;...   150 kV, central
        19.21 32.02 18.56 18.56 26.48 20.94 22.48 25.55 23.19 22.01 20.71 22.78 20.72;...   150 kV, peripheral
        0.74 1.23 0.71 0.71 1.02 0.81 0.86 0.98 0.89 0.85 0.80 0.88 0.80;...                Sn 100 kV, central
        0.69 1.15 0.67 0.67 0.95 0.75 0.81 0.92 0.84 0.79 0.75 0.82 0.75;...                Sn 100 kV, peripheral
        4.78 7.97 4.62 4.62 6.59 5.21 5.59 6.36 5.77 5.48 5.15 5.67 5.16;...                Sn 150 kV, central
        4.60 7.67 4.44 4.44 6.34 5.01 5.38 6.12 5.55 5.27 4.96 5.45 4.96;...                Sn 150 kV, peripheral
        
        0.58 0.97 0.56 0.56 0.80 0.64 0.68 0.78 0.70 0.67 0.63 0.69 0.63;...                70 kV, central, standard filter, 32 cm phantom
        1.34 2.23 1.29 1.29 1.84 1.46 1.56 1.78 1.61 1.53 1.44 1.59 1.44;...                70 kV, peripheral
        1.03 1.72 1.00 1.00 1.42 1.12 1.21 1.37 1.24 1.18 1.11 1.22 1.11;...                80 kV, central
        2.16 3.59 2.08 2.08 2.97 2.35 2.52 2.87 2.60 2.47 2.32 2.56 2.33;...                80 kV, peripheral
        1.62 2.69 1.56 1.56 2.23 1.76 1.89 2.15 1.95 1.85 1.74 1.92 1.74;...                90 kV, central
        3.21 5.35 3.10 3.10 4.42 3.50 3.76 4.27 3.88 3.68 3.46 3.81 3.46;...                90 kV, peripheral
        2.32 3.86 2.24 2.24 3.19 2.52 2.71 3.08 2.80 2.65 2.50 2.75 2.50;...                100 kV, central
        4.42 7.37 4.27 4.27 6.09 4.82 5.17 5.88 5.34 5.07 4.77 5.24 4.77;...                100 kV, peripheral
        3.11 5.19 3.01 3.01 4.29 3.39 3.64 4.14 3.76 3.57 3.36 3.69 3.36;...                110 kV, central
        5.71 9.51 5.51 5.51 7.86 6.22 6.68 7.59 6.89 6.54 6.15 6.77 6.16;...                110 kV, peripheral
        4.02 6.69 3.88 3.88 5.53 4.38 4.70 5.34 4.85 4.60 4.33 4.76 4.33;...                120 kV, central
        7.30 12.16 7.05 7.05 10.05 7.95 8.53 9.70 8.81 8.36 7.86 8.65 7.87;...              120 kV, peripheral
        4.97 8.28 4.80 4.80 6.84 5.41 5.81 6.60 5.99 5.69 5.35 5.89 5.36;...                130 kV, central
        8.88 14.80 8.58 8.58 12.24 9.68 10.39 11.81 10.72 10.18 9.57 10.53 9.58;...         130 kV, peripheral
        5.98 9.97 5.78 5.78 8.25 6.52 7.00 7.96 7.23 6.86 6.45 7.10 6.46;...                140 kV, central
        10.58 17.63 10.22 10.22 14.58 11.53 12.37 14.07 12.77 12.12 11.40 12.54 11.41;...   140 kV, peripheral
        7.07 11.79 6.83 6.83 9.75 7.71 8.28 9.41 8.54 8.11 7.62 8.39 7.63;...               150 kV, central
        12.37 20.62 11.96 11.96 17.05 13.48 14.48 16.46 14.94 14.18 13.34 14.67 13.35;...   150 kV, peripheral
        0.28 0.47 0.27 0.27 0.38 0.30 0.33 0.37 0.34 0.32 0.30 0.33 0.30;...                Sn 100 kV, central
        0.45 0.75 0.43 0.43 0.62 0.49 0.53 0.60 0.54 0.52 0.49 0.53 0.49;...                Sn 100 kV, peripheral
        1.96 3.27 1.89 1.89 2.70 2.14 2.29 2.61 2.37 2.25 2.11 2.32 2.11;...                Sn 150 kV, central
        3.08 5.13 2.97 2.97 4.24 3.35 3.60 4.09 3.71 3.53 3.32 3.65 3.32;...                Sn 150 kV, peripheral
        
        0.51 0.84 0.49 0.49 0.70 0.55 0.59 0.67 0.61 0.58 0.54 0.60 0.55;...                70 kV, central, narrow filter, 32 cm phantom
        1.00 1.66 0.96 0.96 1.38 1.09 1.17 1.33 1.21 1.14 1.08 1.18 1.08;...                70 kV, peripheral
        0.90 1.49 0.87 0.87 1.23 0.98 1.05 1.19 1.08 1.03 0.97 1.06 0.97;...                80 kV, central
        1.59 2.66 1.54 1.54 2.20 1.74 1.87 2.12 1.93 1.83 1.72 1.89 1.72;...                80 kV, peripheral
        1.40 2.34 1.35 1.35 1.93 1.53 1.64 1.87 1.69 1.61 1.51 1.66 1.51;...                90 kV, central
        2.44 4.07 2.36 2.36 3.37 2.66 2.86 3.25 2.95 2.80 2.63 2.90 2.63;...                90 kV, peripheral
        2.04 3.41 1.98 1.98 2.82 2.23 2.39 2.72 2.47 2.34 2.20 2.42 2.21;...                100 kV, central
        3.38 5.64 3.27 3.27 4.66 3.69 3.96 4.50 4.09 3.88 3.65 4.01 3.65;...                100 kV, peripheral
        2.75 4.59 2.66 2.66 3.79 3.00 3.22 3.66 3.32 3.15 2.97 3.26 2.97;...                110 kV, central
        4.45 7.42 4.30 4.30 6.14 4.85 5.21 5.92 5.38 5.10 4.80 5.28 4.80;...                110 kV, peripheral
        3.56 5.94 3.44 3.44 4.91 3.88 4.17 4.74 4.30 4.08 3.84 4.23 3.85;...                120 kV, central
        5.65 9.41 5.46 5.46 7.78 6.16 6.61 7.51 6.82 6.47 6.09 6.70 6.09;...                120 kV, peripheral
        4.42 7.37 4.27 4.27 6.09 4.82 5.17 5.88 5.34 5.07 4.77 5.24 4.77;...                130 kV, central
        6.91 11.52 6.68 6.68 9.52 7.53 8.08 9.19 8.34 7.92 7.45 8.19 7.45;...               130 kV, peripheral
        5.34 8.90 5.16 5.16 7.36 5.82 6.25 7.10 6.45 6.12 5.76 6.33 5.76;...                140 kV, central
        8.26 13.76 7.98 7.98 11.38 9.00 9.66 10.98 9.97 9.46 8.90 9.79 8.91;...             140 kV, central
        6.33 10.56 6.12 6.12 8.73 6.90 7.41 8.42 7.65 7.26 6.83 7.51 6.83;...               150 kV, central
        9.68 16.13 9.35 9.35 13.34 10.55 11.32 12.88 11.69 11.09 10.43 11.48 10.44;...      150 kV, peripheral
        0.25 0.42 0.24 0.24 0.35 0.27 0.29 0.33 0.30 0.29 0.27 0.30 0.27;...                Sn 100 kV, central
        0.36 0.59 0.34 0.34 0.49 0.39 0.42 0.47 0.43 0.41 0.38 0.42 0.38;...                Sn 100 kV, peripheral
        1.78 2.97 1.72 1.72 2.46 1.94 2.09 2.37 2.15 2.04 1.92 2.12 1.92;...                Sn 150 kV, central
        2.50 4.17 2.42 2.42 3.45 2.72 2.93 3.33 3.02 2.87 2.70 2.97 2.70;...                Sn 150 kV, peripheral
        ];
    
%Reshape the table
tab = reshape(tab,[length(positions) length(KVPs) length(filters) length(phantoms) length(configs)]);

%Scale the table to be per mAs
tab = tab/100;

end