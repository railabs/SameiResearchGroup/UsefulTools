% Copyright 20xx - 2019. Duke University
function varargout = getGitCommitID(input,varargin)
%Gets the SHA-1 checksum of the latest commit of a git repository
%
% id = getGitCommitID(input) finds the git SHA-1 checksum for the latest
% commit of a git repository. input is the path to the repository. The
% SHA-1 check sum serves as a unique identifier for a commit and thus can
% be used as a kind of version number. If the input directory is not a git
% repository or the function has trouble finding the SHA-1 string, the
% function gives a warning and returns id as an empty string
%
% [id,branchName] = getGitCommitID(input) additionally returns the branch
% name.

% Written by Justin Solomon
% based on FEX: https://www.mathworks.com/matlabcentral/fileexchange/32864-get-git-info

if length(varargin)>1
    verbose = varargin{1};
else
    verbose = false;
end

%Check if input exists
if ~isdir(input)
    if verbose
        warning('input directory not found');
    end
    id = '';
    branchName = '';
    %Assign outputs
    switch nargout
        case 1
            varargout{1} = id;
        case 2
            varargout{1} = id;
            varargout{2} = branchName;
    end
    return
end

%Initialize id as empty string
id = '';
branchName='';

%Add file sep to the end of input if needed
if ~strcmp(input(end),filesep)
    input = [input filesep];
end

%Check if input is a git repository
if ~isdir([input '.git']) || ~exist([input '.git' filesep 'HEAD'],'file')
    warning('input does not appear to be a git repository, returning empty id');
    return
end

%Read HEAD info
text=fileread([input '.git/HEAD']);
parsed=textscan(text,'%s');
if ~strcmp(parsed{1}{1},'ref:') || ~length(parsed{1})>1
        warning('HEAD file is not in expected format');
        return
end

path=parsed{1}{2};
[pathstr, name, ext]=fileparts(path);
branchName=name;

%Read in SHA1
SHA1text=fileread(fullfile([input '.git' filesep pathstr],[name ext]));
SHA1=textscan(SHA1text,'%s');
id=SHA1{1}{1};

%Assign outputs
switch nargout
    case 1
        varargout{1} = id;
    case 2
        varargout{1} = id;
        varargout{2} = branchName;
end