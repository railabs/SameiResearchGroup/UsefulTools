% Copyright 20xx - 2019. Duke University
function CTDI_vol = getHD750CTDIvol(MA,s,BW,SFOV,KVP,pitch)


%Make all the tables I need;
SFOVs= {'Ped Head','Ped Body','Small Head','Head','Small Body','Medium Body','Large Body','Cardiac Small','Cardiac Medium','Cardiac Large'}';

refs = [41.57 42.36;...
        41.57 42.36;...
        41.57 42.36;...
        43.90 48.91;...
        11.24 21.74;...
        12.31 26.25;...
        11.71 25.45;...
        11.24 21.74;...
        12.31 26.25;...
        11.71 25.45];
refTable.Center = refs(:,1);
refTable.Peripheral = refs(:,2);


KVTable_C = [   80  .34 .34 .34 .34 .28 .28 .28 .28 .28 .28; ...
                100 .64 .64 .64 .64 .59 .59 .59 .59 .59 .59;  ...
                120  1  1   1   1   1   1   1   1   1   1;   ...
                140  1.41 1.41 1.41 1.41 1.49 1.49 1.49 1.49 1.49 1.49];
            
KVTable_P = [   80  .37 .37 .37 .37 .35 .35 .35 .35 .35 .35; ...
                100 .66 .66 .66 .66 .64 .64 .64 .64 .64 .64;  ...
                120  1  1   1   1   1   1   1   1   1   1;   ...
                140  1.39 1.39 1.39 1.39 1.41 1.41 1.41 1.41 1.41 1.41];
            
BWTable_C_S = [ 40   1   1   1   1   1   1   1   1   1   1; ...         %This is for the small focal spot
                20   1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1;  ...
                10   1.2 1.2 1.2 1.2 1.2 1.2 1.2 1.2 1.2 1.2;   ...
                5    1.5 1.5 1.5 1.5 1.5 1.5 1.5 1.5 1.5 1.5;   ...
                2.5  1.7 1.7 1.7 1.7 1.6 1.6 1.7 1.6 1.6 1.7;   ...
                1.25 2.4 2.4 2.4 2.4 2.4 2.3 2.4 2.4 2.3 2.4];
            
BWTable_P_S = [ 40   1   1   1   1   1   1   1   1   1   1; ...
                20   1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1;  ...
                10   1.2 1.2 1.2 1.2 1.2 1.3 1.2 1.2 1.3 1.2;   ...
                5    1.5 1.5 1.5 1.5 1.5 1.5 1.5 1.5 1.5 1.5;   ...
                2.5  1.7 1.7 1.7 1.7 1.7 1.7 1.7 1.7 1.7 1.7;   ...
                1.25 2.4 2.4 2.4 2.4 2.6 2.5 2.5 2.6 2.5 2.5];
            
BWTable_C_L = [ 40   1   1   1   1   1   1   1   1   1   1; ...     %This is for the large focal spot
                20   1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1;  ...
                10   1.3 1.3 1.3 1.3 1.3 1.3 1.3 1.3 1.3 1.3;   ...
                5    1.6 1.6 1.6 1.6 1.6 1.6 1.6 1.6 1.6 1.6;   ...
                2.5  1.7 1.7 1.7 1.7 1.6 1.7 1.6 1.6 1.7 1.6;   ...
                1.25 2.4 2.4 2.4 2.4 2.4 2.4 2.3 2.4 2.4 2.3];
            
BWTable_P_L = [ 40   1   1   1   1   1   1   1   1   1   1; ...
                20   1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1;  ...
                10   1.3 1.3 1.3 1.3 1.3 1.3 1.3 1.3 1.3 1.3;   ...
                5    1.6 1.6 1.6 1.6 1.7 1.7 1.7 1.7 1.7 1.7;   ...
                2.5  1.7 1.7 1.7 1.7 1.7 1.7 1.7 1.7 1.7 1.7;   ...
                1.25 2.5 2.5 2.5 2.5 2.5 2.5 2.5 2.5 2.5 2.5];
            
            
%Get the reference doses for center and periphery
ind = find(strcmp(SFOV,SFOVs));
Ref_CTDI100_C = refTable.Center(ind);
Ref_CTDI100_P = refTable.Peripheral(ind);

%Get the KVP adjustment factors
ind_KVP = find(KVTable_C(:,1) == KVP);
A_KVP_C = KVTable_C(ind_KVP,ind+1);
A_KVP_P = KVTable_P(ind_KVP,ind+1);

%Get the Beam Width Adjustment factor
FS = getFocalSpotSize(KVP,MA);
ind_BW = find(BWTable_C_S(:,1) == BW);
switch FS
    case 'SMALL'
        A_BW_C = BWTable_C_S(ind_BW,ind+1);
        A_BW_P = BWTable_P_S(ind_BW,ind+1);
    case 'LARGE'
        A_BW_C = BWTable_C_L(ind_BW,ind+1);
        A_BW_P = BWTable_P_L(ind_BW,ind+1);
end

%Get the MAS adjustment factor
A_MA = MA*s/260;

%Compute the Center CTDI_100;
CTDI_100_C = Ref_CTDI100_C * A_KVP_C * A_BW_C * A_MA;
%Compute the Peripheral CTDI_100;
CTDI_100_P = Ref_CTDI100_P * A_KVP_P * A_BW_P * A_MA;

%Compute weighted CTDI
CTDI_w = (1/3)*CTDI_100_C + (2/3)*CTDI_100_P;

%Compute the CTDI_vol
CTDI_vol = CTDI_w/pitch;

end

function FS = getFocalSpotSize(KVP,MA)

switch KVP
    case 80
        if MA <620
            FS = 'SMALL';
        else
            FS = 'LARGE';
        end
    case 100
        if MA <680
            FS = 'SMALL';
        else
            FS = 'LARGE';
        end
    case 120
        if MA <570
            FS = 'SMALL';
        else
            FS = 'LARGE';
        end
    case 140
        if MA <490
            FS = 'SMALL';
        else
            FS = 'LARGE';
        end
    otherwise
        FS = 'SMALL';
end

end
