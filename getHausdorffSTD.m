% Copyright 20xx - 2019. Duke University
function [HDmean, HDstd, HDmap, HDlist] = getHausdorffSTD(file1,file2,nVox,varargin)
%This function measures the local Hausdorff distance of two mesh files,
%file1 and file2 using nVox number of voxels in each dimension.
%--------------------------------------------------------------------------
%Syntax:
%   [HDmean, HDstd, HDmap, HDlist] = getHausdorffSTD(file1,file2,nVox)
%   computes the local Hausdorff distance between two meshes (in .stl
%   format) and returns HDmean, the mean distance, HDstd, the standard
%   deviation of the distances across all voxels, HDmap, a 3D map of the
%   HD values, and HDlist, a list of the HD values (for voxels where both
%   meshes had vertices).
%
%   [...] = getHausdorffSTD(...,name,value,...) uses optional settings to
%   compute the HD.
%--------------------------------------------------------------------------
%Optional settings
%
%   DownSample
%   Downsample factor for the meshes. Any value less than 1 reduces the
%   number of vertices in the mesh by that factor (i.e., .5 reduces by
%   half). Default is 1 (i.e., no downsampling)
%
%   RemoveDuplicates
%   Boolean controlling if duplicate vertices are removed from the meshes
%   before computing the HD. Default is true.
%       
%   Shift
%   Boolean controls if the meshes are shifted to the origing using their
%   centroids. Default is true.
%
%   Rotation
%   1x3 vector describing the rotation (in degrees) about each axis for the
%   mesh in file2. Default is [0 0 0] (i.e., no rotation).
%--------------------------------------------------------------------------
%Notes
%   This code was written by Justin Solomon but was adapted from code
%   written by Pooyan Sahbaee and Marthony Robins.
%
%   version 1.0 - 10/04/15
%--------------------------------------------------------------------------

%Parse the extra inputs to get settings
[downsample, removeDuplicates, shift, rot] =  parseInputs(varargin);

%read in the two STL files
[Fp,P,~]=Hausdorff_stlread(file1);
[Fq,Q,~]=Hausdorff_stlread(file2);

%reduce triangles if needed
if downsample<1 && downsample>0
    [P,~]=meshresample(P,Fp,downsample);
    [Q,~]=meshresample(Q,Fq,downsample);
end

%remove duplicates
if removeDuplicates
    P = unique(P,'rows');
    Q = unique(Q,'rows');
end

%shift the meshes if needed
if shift
    centerP = mean(P);
    centerQ = mean(Q);
    P = P-repmat(centerP,[size(P,1) 1]);
    Q = Q-repmat(centerQ,[size(Q,1) 1]);
end

%rotate Q if needed
if ~isequal(rot,[0 0 0])
    Rx = rotx(rot(1));
    % About the y-axis:
    Ry = roty(rot(2));
    % About the z-axis:
    Rz = rotz(rot(3));
    R = Rx*Ry*Rz;
    Q = Q*R;
end

%Bin the points in P and Q;
[Pi, PhasPoints] = binMesh(P,nVox);
[Qi, QhasPoints] = binMesh(Q,nVox);

%get voxels that had points in both meshes
hasPoints = PhasPoints & QhasPoints;

Pi = Pi(hasPoints);
Qi = Qi(hasPoints);

%Loop through list of voxels and compute the local H-distance
for i=1:size(Pi,1)
    pi = Pi{i}; qi = Qi{i};
    hd(i) = getLocalHD(pi,qi,P,Q);
end
HDstd = std(hd);
HDmean = mean(hd);
HDlist = hd;

%make HD map
HDmap = zeros(nVox,nVox,nVox);
HDmap(hasPoints) = hd;

end

function [downsample, removeDuplicates, shift, rot] =  parseInputs(args)

%Default settings
downsample = 1;
removeDuplicates = true;
shift = true;
rot = [0 0 0];

if ~isempty(args)
    for i=1:2:length(args)
        switch args{i}
            case 'DownSample'
                downsample = args{i+1};
            case 'RemoveDuplicates'
                removeDuplicates = args{i+1};
            case 'Shift'
                shift = args{i+1};
            case 'Rotation'
                rot = args{i+1};  
        end
    end
    
end

end

function HD = getLocalHD(pi,qi,P,Q)
%make grid
[Pi,Qi] = meshgrid(pi,qi);

%Compute distance between each point
Pp = [P(Pi,1) P(Pi,2) P(Pi,3)];
Qq = [Q(Qi,1) Q(Qi,2) Q(Qi,3)];
HD =  sum((Pp-Qq).^2,2);
HD = reshape(HD,size(Pi));
HD=max(min(HD));

end

function [Pi,hasPoints] = binMesh(P,nVox)

%get bounding box
Pmin = min(P);
Pmax = max(P);

%create coordinate system (These points describe the center of each voxel)
x = linspace(Pmin(1),Pmax(1),nVox+1); dx = x(2)-x(1); x = x(1:end-1)+dx/2;
y = linspace(Pmin(2),Pmax(2),nVox+1); dy = y(2)-y(1); y = y(1:end-1)+dy/2;
z = linspace(Pmin(3),Pmax(3),nVox+1); dz = z(2)-z(1); z = z(1:end-1)+dz/2;

%Initialize stuff
hasPoints = false(nVox,nVox,nVox);
Pi = cell(nVox,nVox,nVox);

%loop through points in put them into bins
for i=1:size(P,1)
    %find the voxel containing the point
    [~,indx]=min(abs(P(i,1)-x));
    [~,indy]=min(abs(P(i,2)-y));
    [~,indz]=min(abs(P(i,3)-z));
    
    %append the index of that point into the cell array 
    Pi{indy,indx,indz}(end+1,1)=i;
    
    %Switch the logical voxel array to true for that point
    hasPoints(indy,indx,indz)=true;
end



end

function varargout = Hausdorff_stlread(file)
% STLREAD imports geometry from an STL file into MATLAB.
%    FV = STLREAD(FILENAME) imports triangular faces from the ASCII or binary
%    STL file idicated by FILENAME, and returns the patch struct FV, with fields
%    'faces' and 'vertices'.
%
%    [F,V] = STLREAD(FILENAME) returns the faces F and vertices V separately.
%
%    [F,V,N] = STLREAD(FILENAME) also returns the face normal vectors.
%
%    The faces and vertices are arranged in the format used by the PATCH plot
%    object.

% Copyright 2011 The MathWorks, Inc.

    if ~exist(file,'file')
        error(['File ''%s'' not found. If the file is not on MATLAB''s path' ...
               ', be sure to specify the full path to the file.'], file);
    end
    
    fid = fopen(file,'r');    
    if ~isempty(ferror(fid))
        error(lasterror); %ok
    end
    
    M = fread(fid,inf,'uint8=>uint8');
    fclose(fid);
    
    [f,v,n] = stlbinary(M);
    
    varargout = cell(1,nargout);
    switch nargout        
        case 2
            varargout{1} = f;
            varargout{2} = v;
        case 3
            varargout{1} = f;
            varargout{2} = v;
            varargout{3} = n;
        otherwise
            varargout{1} = struct('faces',f,'vertices',v);
    end

end

function [F,V,N] = stlbinary(M)

    F = [];
    V = [];
    N = [];
    
    if length(M) < 84
        error('MATLAB:stlread:incorrectFormat', ...
              'Incomplete header information in binary STL file.');
    end
    
    % Bytes 81-84 are an unsigned 32-bit integer specifying the number of faces
    % that follow.
    numFaces = typecast(M(81:84),'uint32');
    %numFaces = double(numFaces);
    if numFaces == 0
        warning('MATLAB:stlread:nodata','No data in STL file.');
        return
    end
    
    T = M(85:end);
    F = NaN(numFaces,3);
    V = NaN(3*numFaces,3);
    N = NaN(numFaces,3);
    
    numRead = 0;
    while numRead < numFaces
        % Each facet is 50 bytes
        %  - Three single precision values specifying the face normal vector
        %  - Three single precision values specifying the first vertex (XYZ)
        %  - Three single precision values specifying the second vertex (XYZ)
        %  - Three single precision values specifying the third vertex (XYZ)
        %  - Two unused bytes
        i1    = 50 * numRead + 1;
        i2    = i1 + 50 - 1;
        facet = T(i1:i2)';
        
        n  = typecast(facet(1:12),'single');
        v1 = typecast(facet(13:24),'single');
        v2 = typecast(facet(25:36),'single');
        v3 = typecast(facet(37:48),'single');
        
        n = double(n);
        v = double([v1; v2; v3]);
        
        % Figure out where to fit these new vertices, and the face, in the
        % larger F and V collections.        
        fInd  = numRead + 1;        
        vInd1 = 3 * (fInd - 1) + 1;
        vInd2 = vInd1 + 3 - 1;
        
        V(vInd1:vInd2,:) = v;
        F(fInd,:)        = vInd1:vInd2;
        N(fInd,:)        = n;
        
        numRead = numRead + 1;
    end
    
end

function [F,V,N] = stlascii(M)
    warning('MATLAB:stlread:ascii','ASCII STL files currently not supported.');
    F = [];
    V = [];
    N = [];
end

function tf = isbinary(A)
% ISBINARY uses the first line of an STL file to identify its format.
    if isempty(A) || length(A) < 5
        error('MATLAB:stlread:incorrectFormat', ...
              'File does not appear to be an ASCII or binary STL file.');
    end    
    if strcmpi('solid',char(A(1:5)'))
        tf = false; % ASCII
    else
        tf = true;  % Binary
    end
end

function [node,elem]=meshresample(v,f,keepratio)
%
% [node,elem]=meshresample(v,f,keepratio)
%
% resample mesh using CGAL mesh simplification utility
%
% author: Qianqian Fang (fangq<at> nmr.mgh.harvard.edu)
% date: 2007/11/12
%
% input:
%    v: list of nodes
%    f: list of surface elements (each row for each triangle)
%    keepratio: decimation rate, a number less than 1, as the percentage
%               of the elements after the sampling
%
% output:
%    node: the node coordinates of the sampled surface mesh
%    elem: the element list of the sampled surface mesh
%
% -- this function is part of iso2mesh toolbox (http://iso2mesh.sf.net)
%

[node,elem]=domeshsimplify(v,f,keepratio);

if(length(node)==0)
    warning(['Your input mesh contains topological defects, and the ',...
           'mesh resampling utility aborted during processing. Now iso2mesh ',...
           'is trying to repair your mesh with meshcheckrepair. ',...
           'You can also call this manually before passing your mesh to meshresample.'] );
    [vnew,fnew]=meshcheckrepair(v,f);
    [node,elem]=domeshsimplify(vnew,fnew,keepratio);
end
[node,I,J]=unique(node,'rows');
elem=J(elem);
saveoff(node,elem,mwpath('post_remesh.off'));

end

function [node,elem]=domeshsimplify(v,f,keepratio)
  exesuff=getexeext;
  exesuff=fallbackexeext(exesuff,'cgalsimp2');

  saveoff(v,f,mwpath('pre_remesh.off'));
  deletemeshfile(mwpath('post_remesh.off'));
  system([' "' mcpath('cgalsimp2') exesuff '" "' mwpath('pre_remesh.off') '" ' num2str(keepratio) ' "' mwpath('post_remesh.off') '"']);
  [node,elem]=readoff(mwpath('post_remesh.off'));
end
