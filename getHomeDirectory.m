% Copyright 20xx - 2019. Duke University
function userdir = getHomeDirectory
%This function returns the users's home directory

if ispc; userdir= getenv('USERPROFILE'); 
else; userdir= getenv('HOME'); 
end

userdir = [userdir filesep];

end