% Copyright 20xx - 2019. Duke University
function R2 = getR2(x,y)
%this function computes the coefficient of determination (R squared) for a
%linear regression between x and y.
p = polyfit(x,y,1);
yfit = polyval(p,x);
yresid = y - yfit;
SSresid = sum(yresid.^2);
SStotal = (length(y)-1) * var(y);
R2 = 1 - SSresid/SStotal;
end