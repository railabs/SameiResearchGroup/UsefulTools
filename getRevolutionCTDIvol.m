% Copyright 20xx - 2019. Duke University
function CTDI_vol = getRevolutionCTDIvol(MA,s,BW,SFOV,KVP,pitch,FocalSpot)

refmAs = 400;

%Make all the tables I need;
SFOVs= {'Small Head','Ped Head','Head','Ped Body','Small Body','Medium Body','Large Body','Cardiac Small','Cardiac Medium','Cardiac Large'}';


refs = [52.65 53.79;...
        52.65 53.79;...
        56.01 62.67;...
        15.19 29.72;...
        15.19 29.72;...
        16.75 35.23;...
        15.74 32.95;...
        15.19 29.72;...
        16.75 35.23;...
        15.74 32.95;...
        ];
    
refTable.Center = refs(:,1);
refTable.Peripheral = refs(:,2);


KVTable_C = [   70  .22 .22 .22 .16 .16 .17 .15 .16 .17 .15; ...
                80  .33 .33 .34 .28 .28 .28 .26 .28 .28 .26; ...
                100 .63 .63 .64 .59 .59 .59 .58 .59 .59 .58;  ...
                120  1  1   1   1   1   1   1   1   1   1;   ...
                140 1.42 1.42 1.42 1.5 1.5 1.49 1.52 1.5 1.49 1.52];
            
KVTable_P = [   70  .24 .24 .25 .24 .24 .24 .21 .24 .24 .21;...
                80  .36 .36 .37 .35 .35 .36 .33 .35 .36 .33; ...
                100 .65 .65 .66 .64 .64 .65 .63 .64 .65 .63;  ...
                120  1  1   1   1   1   1   1   1   1   1;   ...
                140  1.4 1.4 1.4 1.42 1.42 1.41 1.44 1.42 1.41 1.44];
            
BWTable_C_S = [ 5   1.51 1.51 1.51 1.51 1.51 1.5 1.5 1.51 1.5 1.5; ...         %This is for the small focal spot
                40   1  1   1   1   1   1   1   1   1   1;  ...
                80   1  1   1   1   1   .99   1   1   .99   1;   ...
                120  .96 .96 .96 .96 .96 .96 .96 .96 .96 .96;   ...
                140  .94 .94 .94 .94 .94 .94 .94 .94 .94 .94;   ...
                160  .94 .94 .94 .93 .93 .94 .94 .93 .94 .94];
            
BWTable_P_S = [ 5    1.5 1.5 1.5 1.49 1.49 1.49 1.49 1.49 1.49 1.49; ...        
                40   1  1   1   1   1   1   1   1   1   1;  ...
                80   .99 .99 .99 .99 .99 .99 .99 .99 .99 .99;   ...
                120  .95 .95 .96 .95 .95 .95 .95 .95 .95 .95;   ...
                140  .94 .94 .94 .93 .93 .93 .94 .93 .93 .94;   ...
                160  .93 .93 .93 .93 .93 .93 .93 .93 .93 .93];
            
BWTable_C_L = [ 5   1.65 1.65 1.65 1.65 1.65 1.65 1.65 1.65 1.65 1.65; ...         %This is for the large focal spot
                40   1  1   1   1   1   1   1   1   1   1;  ...
                80    1  1   1   1   1   1   1   1   1   1;   ...
                120  .96 .96 .96 .96 .96 .96 .97 .96 .96 .97;   ...
                140  .94 .94 .94 .94 .94 .94 .95 .94 .94 .95;   ...
                160  .94 .94 .94 .94 .94 .94 .94 .94 .94 .94];
            
BWTable_P_L = [ 5    1.64 1.64 1.64 1.63 1.63 1.63 1.63 1.63 1.63 1.63; ...        
                40   1  1   1   1   1   1   1   1   1   1;  ...
                80   .99 .99 .99 .99 .99 .99 .99 .99 .99 .99;   ...
                120  .96 .96 .96 .95 .95 .95 .96 .95 .95 .96;   ...
                140  .94 .94 .94 .93 .93 .93 .94 .93 .93 .94;   ...
                160  .93 .93 .93 .93 .93 .93 .93 .93 .93 .93];

            
            
BWTable_C_XL = [ 5   1.65 1.65 1.65 1.65 1.65 1.65 1.65 1.65 1.65 1.65; ...         %This is for the Xtra large focal spot
                40   1  1   1   1   1   1   1   1   1   1;  ...
                80    1  1   1   1   1   1   1   1   1   1;   ...
                120  .96 .96 .96 .96 .96 .96 .96 .96 .96 .96;   ...
                140  .94 .94 .94 .94 .94 .94 .95 .94 .94 .95;   ...
                160  .94 .94 .94 .94 .94 .94 .94 .94 .94 .94];
            
BWTable_P_XL = [ 5    1.64 1.64 1.64 1.64 1.64 1.64 1.64 1.64 1.64 1.64; ...        
                40   1  1   1   1   1   1   1   1   1   1;  ...
                80   .99 .99 .99 .99 .99 .99 .99 .99 .99 .99;   ...
                120  .96 .96 .96 .95 .95 .95 .96 .95 .95 .96;   ...
                140  .94 .94 .94 .94 .94 .94 .94 .94 .94 .94;   ...
                160  .93 .93 .93 .93 .93 .93 .93 .93 .93 .93];
            
            
%Get the reference doses for center and periphery
ind = find(strcmp(SFOV,SFOVs));
Ref_CTDI100_C = refTable.Center(ind);
Ref_CTDI100_P = refTable.Peripheral(ind);

%Get the KVP adjustment factors
ind_KVP = find(KVTable_C(:,1) == KVP);
A_KVP_C = KVTable_C(ind_KVP,ind+1);
A_KVP_P = KVTable_P(ind_KVP,ind+1);

%Get the Beam Width Adjustment factor
FS = FocalSpot;
%FS = getFocalSpotSize(KVP,MA);
ind_BW = find(BWTable_C_S(:,1) == BW);
switch FS
    case 'Small'
        A_BW_C = BWTable_C_S(ind_BW,ind+1);
        A_BW_P = BWTable_P_S(ind_BW,ind+1);
    case 'Large'
        A_BW_C = BWTable_C_L(ind_BW,ind+1);
        A_BW_P = BWTable_P_L(ind_BW,ind+1);
    case 'XtraLarge'
        A_BW_C = BWTable_C_XL(ind_BW,ind+1);
        A_BW_P = BWTable_P_XL(ind_BW,ind+1);
end

%Get the MAS adjustment factor
A_MA = MA*s/refmAs;

%Compute the Center CTDI_100;
CTDI_100_C = Ref_CTDI100_C * A_KVP_C * A_BW_C * A_MA;
%Compute the Peripheral CTDI_100;
CTDI_100_P = Ref_CTDI100_P * A_KVP_P * A_BW_P * A_MA;

%Compute weighted CTDI
CTDI_w = (1/3)*CTDI_100_C + (2/3)*CTDI_100_P;

%Compute the CTDI_vol
CTDI_vol = CTDI_w/pitch;

end

% function FS = getFocalSpotSize(KVP,MA)
% 
% switch KVP
%     case 80
%         if MA <620
%             FS = 'SMALL';
%         else
%             FS = 'LARGE';
%         end
%     case 100
%         if MA <680
%             FS = 'SMALL';
%         else
%             FS = 'LARGE';
%         end
%     case 120
%         if MA <570
%             FS = 'SMALL';
%         else
%             FS = 'LARGE';
%         end
%     case 140
%         if MA <490
%             FS = 'SMALL';
%         else
%             FS = 'LARGE';
%         end
%     otherwise
%         FS = 'SMALL';
% end
% 
% end
