% Copyright 20xx - 2019. Duke University
function im = getVesselCrossSections(I,centerline,x,y,z,varargin)
%This function gets cross sectional images through a vessel along its
%centerline. Basically it just interpolates from the 3D image volume with
%slices perpendicular to the centerline direction.
%
%--------------------------------------------------------------------------
%Syntax:
%   im = getVesselCrossSections(I,centerline,x,y,z) computes a series of
%   cross-sectional images along the given centerline. I is the input image
%   volume, centerline is a nx3 vector of centerline points, x,y,and z are
%   coordinate vectors of the image (note, length(x) = size(I,2), length(y)
%   = size(I,1), and length(z)=size(I,3)). The resulting image, im, has n-1
%   number of slices. A slice is created at the midway point between the
%   points in centerline. 
%
%   im = getVesselCrossSections(...,name,value,...) lets you specify
%   optional settings as name-value pairs (names are always strings)
%--------------------------------------------------------------------------
%Optional Settings:
%
%   FOV
%   size (in whatever units are used by x,y,z) of cross-sectional images.
%   Default is 40;
%
%   N
%   number of pixels in the cross-sectional images. Default is 50;
%
%   interpMethod
%   'nearest', 'linear' (default), 'spline', 'cubic'
%   this is the interpolation method used. This function uses interp3 to
%   created the cross-sectional images from the image volume.
%
%   showPlot
%   boolean specifying if a 3D plot showing the centerline path and image
%   locations is made. Default is false. This option requires that imtool3D
%   be available.
%--------------------------------------------------------------------------

%Get the extra inputs
[FOV,N,interpMethod,showPlot]=parseinputs(varargin);

%Get the direction vectors
dir = diff(centerline);

%create the pixel coordinates in the vessel coordinate system
xp=linspace(-FOV/2,FOV/2,N);
yp=xp;
[Xp,Yp]=meshgrid(xp,yp);
points = [Xp(:) Yp(:) zeros(size(Xp(:))) ones(size(Xp(:)))]';

%loop over centerline places and compute images
for i=1:size(dir,1)
    %Get the origin
    P0=mean(centerline(i:i+1,:),1);
    
    %Get the z-direction vector (w);
    w = dir(i,:); w=w/norm(w);

    %Make orthonormal basis
    uv=null(w)';
    u=uv(1,:);
    v=uv(2,:);
    
    
    %Make rotation matrix
    R = (flipud(eye(3))*([w;v;u]))';
    
    %add translation matrix
    R=padarray(R,[1,1],'post');
    R(end,end)=1;
    R(1:3,end)=P0';
    
    %Apply transformation matrix
    newPoints = (R*(points))';
    newPoints=newPoints(:,1:3);
    
    %Get corners of the image (used only if the user sets showPlot to be
    %true)
    if showPlot
        locPoints = [-FOV/2 FOV/2 -FOV/2 FOV/2; -FOV/2 -FOV/2 FOV/2 FOV/2;0 0 0 0; 1 1 1 1]; %These are the corners of the vessel image
        locPoints = (R*(locPoints))'; %Transform those corners to the standard image coordinate system
        locs(:,:,i) =locPoints(:,1:3);
    end
        
    %3D interpolation
    vals = interp3(x,y,z,I,newPoints(:,1),newPoints(:,2),newPoints(:,3),interpMethod);
    im(:,:,i) = reshape(vals,[N N]);
    
end

if showPlot
    figure;
    
    %make plot centerline plot
    ha=subplot(1,2,2);
    plot3(centerline(:,1),centerline(:,2),centerline(:,3),'-o'); hold on
    axis equal
    colormap gray
    set(gca,'Xlim',[min(x) max(x)],'Ylim',[min(y) max(y)],'Zlim',[min(z) max(z)])
    set(gca','Color','none','YDir','reverse','XTick',[],'YTick',[],'ZTick',[])
    
    box on
    %Add the planar surface for the image slice being shown
    hs=surface('XData',[min(x) max(x);min(x) max(x)],'YData',[min(y) min(y);max(y) max(y)],'ZData',[z(1) z(1);z(1) z(1)],'CData',I(:,:,1),'FaceColor','texturemap');
    
    %make imtool3D object for full image
    tool = imtool3D(I,[0 0 .5 .5],gcf);
    h=getHandles(tool);
    fun = @(src,evnt) newImageSlice(src,evnt,hs,z,I);
    addlistener(h.Slider,'Value','PostSet',fun);
    
    %make imtool3D object for vessel images
    tool2 = imtool3D(im,[0 .5 .5 .5],gcf,[],tool);
    i=1;
    xdata = [locs(1,1,i) locs(2,1,i); locs(3,1,i) locs(4,1,i)];
    ydata = [locs(1,2,i) locs(2,2,i); locs(3,2,i) locs(4,2,i)];
    zdata = [locs(1,3,i) locs(2,3,i); locs(3,3,i) locs(4,3,i)];
    hs=surface('XData',xdata,'YData',ydata,'ZData',zdata,'CData',im(:,:,i),'FaceColor','texturemap','EdgeColor','r','Parent',ha);
    h=getHandles(tool2);
    fun = @(src,evnt) newVessselImageSlice(src,evnt,hs,locs,im);
    addlistener(h.Slider,'Value','PostSet',fun);
    set(gca,'XColor','w','YColor','w','ZColor','w')
    set(gcf,'Units','normalized','Position',[.1 .1 .8 .8],'Color','k')
    
end

end

function [FOV,N,interpMethod,showPlot]=parseinputs(args)
%Default settings
FOV=40;
N=50;
interpMethod = 'linear';
showPlot=false;
if ~isempty(args)
    for i=1:2:length(args)
        switch args{i}
            case 'FOV'
                FOV = args{i+1};
            case 'interpMethod'
                interpMethod = args{i+1};
            case 'N'
                N=args{i+1};
            case 'showPlot'
                showPlot=args{i+1};
                
        end
    end
    
end

end

function newImageSlice(src,evnt,hs,z,I)
slice = round(get(evnt.AffectedObject,'Value'));
z=z(slice);
set(hs,'ZData',[z z; z z],'CData',I(:,:,slice));

end

function newVessselImageSlice(src,evnt,hs,locs,im)
i = round(get(evnt.AffectedObject,'Value'));
xdata = [locs(1,1,i) locs(2,1,i); locs(3,1,i) locs(4,1,i)];
ydata = [locs(1,2,i) locs(2,2,i); locs(3,2,i) locs(4,2,i)];
zdata = [locs(1,3,i) locs(2,3,i); locs(3,3,i) locs(4,3,i)];
set(hs,'XData',xdata,'YData',ydata,'ZData',zdata,'CData',im(:,:,i));
end
