% Copyright 20xx - 2019. Duke University
function WED = getWEDfromCT(I,psize,varargin)
%Returns the water equivalent diameter (WED) of a CT image
%   This funtion is based on AAPM TG Report 220
%   WED = getWEDfromCT(I,psize) returns the WED from the image stack I.
%   psize is the pixel size. Units of WED are the same as the units of
%   psize (typicaly mm).

%Get the alpha value (default it one, which is what is reccomended in that
%report
[alpha] = parseInputs(varargin);

%Set anything below -1000 to -1000
I(I<-1000)=-1000;

for i=1:size(I,3)
    im = I(:,:,i);
    im = im(:);
    WEA(i,1) = sum(((im/1000+1).^alpha)*(psize.^2));
end

%Reshape the image to be Npix X nSlices (each column is a slice)
%I = reshape(I,[size(I,1)*size(I,2) size(I,3)]);

%Calculate the water equivalent area
%WEA = sum(((I/1000+1).^alpha)*(psize.^2),1)';

%Calculate the WED
WED = 2*sqrt(WEA./pi);
end

function [alpha] = parseInputs(args)
alpha = 1;

if length(args)>1
    for i=1:2:length(args)
        switch args{i}
            case 'alpha'
                alpha = args{i+1};
        end
    end
end
end
