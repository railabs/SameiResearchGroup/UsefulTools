% Copyright 20xx - 2019. Duke University
function varargout = gscatter_special(x,y,S,g,gnames,ax,varargin)
%This function makes a grouped scatter plot with a different legend for
%each grouping variable (this helps when you have many combinations of
%grouping variables, otherwise the legend runs off the figure)
%   

%get categories
cats1 = categories(g{1});
if length(g)==1
    v2Dummy = true;
    g{2} = categorical(repmat({'dummy'},[numel(x) 1]));
else
    v2Dummy = false;
end
cats2 = categories(g{2});

%Define colors
colors = num2cell(linspace(0,1,length(cats1)));

%Define the markers
markers = {'o','s','d','^','v','>','<','p','h'};
if length(markers)<length(cats2)
    n = ceil(length(cats2)/length(markers));
    markers = repmat(markers,[1 n]);
end

if ~isempty(varargin)
    dataLabs = varargin{1};
else
    dataLabs = [];
end


%Rescale the size
if isempty(S)
    S=32*ones(size(x));
else
    S = mat2gray(S)*72 + 8;
end

%Make scatter plots
N = zeros(length(cats1),length(cats2));
for i=1:length(cats1)
    for j=1:length(cats2)
        %Get all the rows in this group
        ind = g{1}==cats1{i} & g{2} == cats2{j};
        N(i,j) = sum(ind);
        %make the scatter plot
        if isnumeric(colors{i})
            C = repmat(colors{i},[numel(x(ind)) 1]);
        else
            C = colors{i};
        end
        h(i,j) = scatter(ax,x(ind),y(ind),S(ind),C,'filled',markers{j},...
            'MarkerEdgeColor','k'); hold on;
        if ~isempty(dataLabs)
            h(i,j).UserData = dataLabs(ind);
        end
    end
end

%make the legend for the first grouping variable
ax1 = copyobj(ax,get(ax,'Parent')); %Copy axes
delete(get(ax1,'Children')); %delete its children
clear htemp
for i=1:length(cats1)
    htemp(i)=scatter(ax1,x(1),y(1),36,colors{i},'filled',markers{1},'MarkerEdgeColor','k');
    leg{i} = [gnames{1} ': ' cats1{i}];
end
L1=legend(ax1,leg,'Location','NorthWest','Interpreter','none');
L1.FontSize = 6;
L1.Color='w';
pos1 = L1.Position;
set(ax1,'Visible','off','Xlim',[min(x)-2 min(x)-1]);

%Make the legend for the second grouping variable
ax2 = copyobj(ax,get(ax,'Parent')); %Copy axes
delete(get(ax2,'Children')); %delete its children
clear leg
clear htemp
for j=1:length(cats2)
    %htemp(j)=scatter(ax2,1,1,36,'k','filled',markers{j},'MarkerEdgeColor','k');
    htemp(j)=scatter(ax2,x(1),y(1),36,markers{j},'MarkerEdgeColor','k');
    leg{j} = [gnames{2} ': ' cats2{j}];
end
L2=legend(ax2,leg,'Location','NorthWest','Interpreter','none');
L2.FontSize=6;
pos2 = L2.Position;
set(ax2,'Visible','off','Xlim',[min(x)-2 min(x)-1]);
%Move location of the second legend
pos2(1) = pos1(1)+pos1(3);
L2.Position=pos2;
L2.Color = 'w';
if v2Dummy
    L2.Visible = 'off';
end

switch nargout
    case 1
        varargout{1} = h;
    case 2
        varargout{1} = h;
        handles.HiddenAx1 = ax1;
        handles.HiddenAx2 = ax2;
        handles.Legend1 = L1;
        handles.Legend2 = L2;
        varargout{2} = handles;
end

%Enable interactive data tips if requested
if ~isempty(dataLabs)
    fig = getParentFigure(ax);
    dcm_obj = datacursormode(fig);
    dcm_obj.Enable = 'on';
    fun = @(dum,event_obj) dataTipUpdateFunction(dum,event_obj,dcm_obj);
    dcm_obj.UpdateFcn = fun;
end
end

function output_txt = dataTipUpdateFunction(~,event_obj,dcm_obj)
% ~            Currently not used (empty)
% event_obj    Object containing event data structure
% output_txt   Data cursor text
pos = event_obj.Position;
[~,ind] = min(sqrt((pos(1)-event_obj.Target.XData).^2 + (pos(2)-event_obj.Target.YData).^2));
lab = event_obj.Target.UserData{ind};
output_txt = lab;
end

