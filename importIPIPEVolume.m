% Copyright 20xx - 2019. Duke University
function [varargout] = importIPIPEVolume(input)
%This function reads in .ipv image volume files (part of Siemen's ipipe
%framework). 
%
%[I] = importIPIPEVolume(input) reads the ipv volume and returns the
%image data only
%
%[I, info] = importIPIPEVolume(input) reads the ipv volume and returns the
%image data along with a structured info variable with metadata

%open the file
id=fopen(input);

%move to location of the footer identifier (21 bytes before the end of the
%file, 13 of those bytes are the string "JsVoxelVolume", the other 8 bytes
%is a 64 bit int specifiying the footer location)
fseek(id,-21,'eof');
footerEndPosition = ftell(id);

%find the beginning of the footer location
footerPosition = fread(id,1,'int64');

%make a temporary XML file (finds a file name that doesn't already exist)
tempExists = true;
i=1;
while tempExists
    fname = ['temp' num2str(i) '.xml'];
    tempExists = exist(fname, 'file') == 2;
    i=i+1;
end
id2 = fopen(fname,'w');

%move to the beginning of the footer
fseek(id,footerPosition,'bof');

%Write out the text to the temp file
position = ftell(id);
while position < footerEndPosition
    tline = fgets(id);
    k = strfind(tline,'</VoxelVolume>');
    if ~isempty(k)
        tline = '</VoxelVolume>';
    end
    fprintf(id2,'%s',tline);
    position = ftell(id);
end
fclose(id2);    %close the xml file
frewind(id);    %rewind to beginning of .ipv file

%read in the temporary xml file
doc = xmlread(fname);

%get the dimensions of the array
info.S = str2num(doc.getElementsByTagName('Dimensions').item(0).getTextContent);

%get the pixel size
info.psize = str2num(doc.getElementsByTagName('VoxelSize').item(0).getTextContent);

%get the pixel size
info.pspacing = str2num(doc.getElementsByTagName('Spacing').item(0).getTextContent);

%compute the FOV
info.FOV = info.S.*info.pspacing;

%Get the origin (i.e., physical position of first voxel)
info.Origin = str2num(doc.getElementsByTagName('Origin').item(0).getTextContent);

%get the rescale value
info.rescale = str2num(doc.getElementsByTagName('DataScale').item(0).getTextContent);
info.rescale = info.rescale(1);

%get the data type
type = char(doc.getDocumentElement.getAttribute('PIXELTYPE'));
switch type
    case 'Single'
        info.type = 'single';
    case 'Double'
        info.type= 'double';
    otherwise
        warning(['Type: ' type 'not recongnized, using single'])
        info.type ='single';
end

%save the input file name
info.fname = input;

%save the xml file into the info variable
info.xml = doc;

%read in the image data
I = fread(id,prod(info.S),info.type); 
I=reshape(I,info.S);
I=permute(I,[2 1 3]);
I = I+info.rescale;

%close the file
fclose(id);

%delete the temp file
delete(fname);

switch nargout
    case 1
        varargout{1} = I;
    case 2
        varargout{1} = I;
        varargout{2} = info;
end


end
