% Copyright 20xx - 2019. Duke University
function [cx cy cz c] = improfile3D(varargin)
%This function returns the pixel values along a specified line segment of a
%3D image.
%--------------------------------------------------------------------------
%Inputs:
% x y z-    row vectors that define the coordinate system of the image.
% I-        3D image. size(I)=[length(y) length(x) length(z)].
% xi yi zi- endpoints of the line segment xi=[x1 x2] yi=[y1 y2] zi=[z1 z2].
% s-        1x3 integar vector giving the interpolation upsample factor 
%           for each dimension. For example s=[2 2 2] divides the voxels in
%           half in each dimension. Default is [1 1 1] i.e., no upsampling.
% method-   type of interpolation ('nearest',{'linear'},'spline','cubic').
%--------------------------------------------------------------------------
%Outputs:
% cx cx cz- spatial coordinates of the points at which the returned pixel
%           values are given.
% c-        vector of pixel values along the specified line.
%           length(c)=length(cx)=length(cy)=length(cz)
%--------------------------------------------------------------------------
%Syntax:
%   [cx cy cz c] = improfile3D(I,xi,yi,zi) takes in input image I and
%   computes the profile, c, of the line given by xi, yi, zi. It also
%   returns the coordinates of the points at which the line profile is
%   measured (cx,cy,cz). Here a default image coordinate system is used.
%
%   [cx cy cz c] = improfile3D(x,y,z,I,xi,yi,zi,n,method) lets you
%   specify a non-default coordinate system with the vectors x,y,z. You can
%   specifiy an upsample factor, n, and interpolation method, 'method'.
%--------------------------------------------------------------------------
%Notes:
%   Written by Justin Solomon, 19 Nov. 2012.
%
%   Everyone has permission to use.
%
%   Current version: 1.0
%
%   Created in MATLAB_R2012a
%
%   Uses a 3D Bresenhem line algorithm based on: B.Pendleton.  "line3d - 3D
%   Bresenham's (a 3D line drawing algorithm)"
%   ftp://ftp.isc.org/pub/usenet/comp.sources.unix/volume26/line3d, 1992.
%   and ported to MATLAB by Jimmy Shen (jimmy@rotman-baycrest.on.ca).
%--------------------------------------------------------------------------

%Check inputs and set defaults
switch nargin
    case 4  %improfile3D(I,xi,yi,zi)
        I=varargin{1}; xi=varargin{2}; yi=varargin{3}; zi=varargin{4};
        x=1:size(I,2); y=1:size(I,1); z=1:size(I,3);
        s=[1 1 1];
        method='linear';
    case 9  %improfile3D(x,y,z,I,xi,yi,zi,s,method)
        x=varargin{1}; y=varargin{2}; z=varargin{3}; I=varargin{4};
        xi=varargin{5}; yi=varargin{6}; zi=varargin{7}; s=round(varargin{8});
        method=varargin{9};
    case 8  %improfile3D(x,y,z,I,xi,yi,zi,s || method)
        x=varargin{1}; y=varargin{2}; z=varargin{3}; I=varargin{4};
        xi=varargin{5}; yi=varargin{6}; zi=varargin{7};
        if isa(varargin{8},'char')
            method=varargin{8};
            s=[1 1 1];
        else
            s=round(varargin{8});
            method='linear';
        end
end

%--------------------------------------------------------------------------

%Define a subsampled coordinates.
psize(1)=abs(x(2)-x(1));
psize(2)=abs(y(2)-y(1));
psize(3)=abs(z(2)-z(1));
nx=x(1):psize(1)/s(1):x(end);
ny=y(1):psize(2)/s(2):y(end);
nz=z(1):psize(3)/s(3):z(end);

%Use Bresenhan's line algorithm to get the coordinates of the line through the interpolated
%subvolume
[m X1]=min(abs(nx-xi(1)));
[m X2]=min(abs(nx-xi(2)));
[m Y1]=min(abs(ny-yi(1)));
[m Y2]=min(abs(ny-yi(2)));
[m Z1]=min(abs(nz-zi(1)));
[m Z2]=min(abs(nz-zi(2)));
[Xp,Yp,Zp] = bresenham_line3d([X1 Y1 Z1], [X2 Y2 Z2],0);

%Return the positions and pixels values of the line
cx=nx(Xp);
cy=ny(Yp);
cz=nz(Zp);
c = interp3(x,y,z,I,cx,cy,cz,method);

function [X,Y,Z] = bresenham_line3d(P1, P2, precision)
    %  Generate X Y Z coordinates of a 3D Bresenham's line between
%  two given points.
%
%  A very useful application of this algorithm can be found in the
%  implementation of Fischer's Bresenham interpolation method in my
%  another program that can rotate three dimensional image volume
%  with an affine matrix:
%  http://www.mathworks.com/matlabcentral/fileexchange/loadFile.do?objectId=21080
%
%  Usage: [X Y Z] = bresenham_line3d(P1, P2, [precision]);
%
%  P1	- vector for Point1, where P1 = [x1 y1 z1]
%
%  P2	- vector for Point2, where P2 = [x2 y2 z2]
%
%  precision (optional) - Although according to Bresenham's line
%	algorithm, point coordinates x1 y1 z1 and x2 y2 z2 should
%	be integer numbers, this program extends its limit to all
%	real numbers. If any of them are floating numbers, you
%	should specify how many digits of decimal that you would
%	like to preserve. Be aware that the length of output X Y
%	Z coordinates will increase in 10 times for each decimal
%	digit that you want to preserve. By default, the precision
%	is 0, which means that they will be rounded to the nearest
%	integer.
%
%  X	- a set of x coordinates on Bresenham's line
%
%  Y	- a set of y coordinates on Bresenham's line
%
%  Z	- a set of z coordinates on Bresenham's line
%
%  Therefore, all points in XYZ set (i.e. P(i) = [X(i) Y(i) Z(i)])
%  will constitute the Bresenham's line between P1 and P1.
%
%  Example:
%	P1 = [12 37 6];     P2 = [46 3 35];
%	[X Y Z] = bresenham_line3d(P1, P2);
%	figure; plot3(X,Y,Z,'s','markerface','b');
%
%  This program is ported to MATLAB from:
%
%  B.Pendleton.  line3d - 3D Bresenham's (a 3D line drawing algorithm)
%  ftp://ftp.isc.org/pub/usenet/comp.sources.unix/volume26/line3d, 1992
%
%  Which is also referenced by:
%
%  Fischer, J., A. del Rio (2004).  A Fast Method for Applying Rigid
%  Transformations to Volume Data, WSCG2004 Conference.
%  http://wscg.zcu.cz/wscg2004/Papers_2004_Short/M19.pdf
%
%  - Jimmy Shen (jimmy@rotman-baycrest.on.ca)
    if ~exist('precision','var') | isempty(precision) | round(precision) == 0
      precision = 0;
      P1 = round(P1);
      P2 = round(P2);
   else
      precision = round(precision);
      P1 = round(P1*(10^precision));
      P2 = round(P2*(10^precision));
   end

   d = max(abs(P2-P1)+1);
   X = zeros(1, d);
   Y = zeros(1, d);
   Z = zeros(1, d);

   x1 = P1(1);
   y1 = P1(2);
   z1 = P1(3);

   x2 = P2(1);
   y2 = P2(2);
   z2 = P2(3);

   dx = x2 - x1;
   dy = y2 - y1;
   dz = z2 - z1;

   ax = abs(dx)*2;
   ay = abs(dy)*2;
   az = abs(dz)*2;

   sx = sign(dx);
   sy = sign(dy);
   sz = sign(dz);

   x = x1;
   y = y1;
   z = z1;
   idx = 1;

   if(ax>=max(ay,az))			% x dominant
      yd = ay - ax/2;
      zd = az - ax/2;

      while(1)
         X(idx) = x;
         Y(idx) = y;
         Z(idx) = z;
         idx = idx + 1;

         if(x == x2)		% end
            break;
         end

         if(yd >= 0)		% move along y
            y = y + sy;
            yd = yd - ax;
         end

         if(zd >= 0)		% move along z
            z = z + sz;
            zd = zd - ax;
         end

         x  = x  + sx;		% move along x
         yd = yd + ay;
         zd = zd + az;
      end
   elseif(ay>=max(ax,az))		% y dominant
      xd = ax - ay/2;
      zd = az - ay/2;

      while(1)
         X(idx) = x;
         Y(idx) = y;
         Z(idx) = z;
         idx = idx + 1;

         if(y == y2)		% end
            break;
         end

         if(xd >= 0)		% move along x
            x = x + sx;
            xd = xd - ay;
         end

         if(zd >= 0)		% move along z
            z = z + sz;
            zd = zd - ay;
         end

         y  = y  + sy;		% move along y
         xd = xd + ax;
         zd = zd + az;
      end
   elseif(az>=max(ax,ay))		% z dominant
      xd = ax - az/2;
      yd = ay - az/2;

      while(1)
         X(idx) = x;
         Y(idx) = y;
         Z(idx) = z;
         idx = idx + 1;

         if(z == z2)		% end
            break;
         end

         if(xd >= 0)		% move along x
            x = x + sx;
            xd = xd - az;
         end

         if(yd >= 0)		% move along y
            y = y + sy;
            yd = yd - az;
         end

         z  = z  + sz;		% move along z
         xd = xd + ax;
         yd = yd + ay;
      end
   end

   if precision ~= 0
      X = X/(10^precision);
      Y = Y/(10^precision);
      Z = Z/(10^precision);
   end

   return;					% bresenham_line3d


