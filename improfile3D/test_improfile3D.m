% Copyright 20xx - 2019. Duke University
clear
x=linspace(-10,10);
[X Y Z]=meshgrid(x); r=sqrt(X.^2+Y.^2+Z.^2);
R=5; n=1;
I=(1-(r./R).^2).^n;
I(r>R)=0;

[cx cy cz c]=improfile3D(x,x,x,I,[-7 7],[-7 7],[-7 7],[1 1 1],'linear');
figure
d=sqrt((cx(1)-cx).^2+(cy(1)-cy).^2+(cz(1)-cz).^2);
plot(d,c)