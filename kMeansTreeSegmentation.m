% Copyright 20xx - 2019. Duke University
function [output] = kMeansTreeSegmentation(I,varargin)
%This function automatically segments a 3D image volume, I into seperate
%classes based on a hierarchical  k-mean clustering method. The algorithm
%works recursivley by diving the image into k-classes at each recursion
%step. The the algorithm is run again on each of these k classes to further
%divide them into sub classes. Loosely based on "Martin, J. A., & H., I.
%(2010). A divisive hierarchical k-means based algorihtm for image
%segmentation. Proceedings of 2010 IEEE International Conference on
%Intelligent Systems and Knowledge Engineering, ISKE 2010, 300?304.
%doi:10.1109/ISKE.2010.5680865" Note this requires the tree class, which is
%available in the Texture.git repo, also from the Matlab file exchange.
%
%Written by Justin Solomon
%v1.0 02/26/16
%
%--------------------------------------------------------------------------
%Syntax:
%   output = kMeansTreeSegmentation(I). I is an nRows X nColumns X nSlices
%   x nLayers image array. Each layer of data can represent different
%   filtering/processing operations that are performed on the data (done to
%   emphasize different features). Usually, the first layer is the original
%   grayscale values and subsequent layers are low-pass or high-pass
%   filtered images (depending on what you really want to segment). output
%   is a structured variable with the results of the segmentation.
%   output.labels is a nRows x nColumns x nSlices label matrix array where
%   each class of segmentation has been assigned an integer index value.
%   output.tree is the tree data structure containing the hierarchy
%   information of segmentation. (The final labels represent the leaves of
%   this tree). output.nLabels is the number of unique values in the labels
%   array (i.e., the number of segmented classes) output.nodeIds is a list
%   of the node ids corresponding to the output labels. The node id is how
%   you access a given node in the tree data structure (see documentation
%   for the tree.m class). For example, output.nodeIds(1) is the node id
%   for label = 0; output.nodeIds(2) for label = 1, etc...
%
%
%   output = kMeansTreeSegmentation(I,name,value,...) lets you specify
%   optional settings as name-value pairs (names are always strings). These
%   options are listed below
%--------------------------------------------------------------------------
%Optional Settings:
%
%   mask
%   binary mask to use at start of algorithm. Use this if you only want to
%   segement a subset of your image data. Sould be same size as the first 3
%   dimensions of I.
%
%   kMax
%   maximum number of branches per node. This controls how many
%   sub-branches can be made from a parent branch. Default is 2. (i.e.,
%   bifurcation).
%
%   gMax
%   maximum number of tree generations. Default is 6. Note that the maximun
%   number of labels is equal to kMax^gMax. 
%
%   verbose
%   boolean specifying if a progess is printed to the command line while
%   the algorithm is running. Because this code works recursively, it is
%   impossible to display progess in a strictly sequential manner. Thus the
%   output is not super useful. But it does give a rough idea of how the
%   code is progessing through the tree.
%--------------------------------------------------------------------------

%get the tree and node id
[T,id,kMax,gMax,verbose]=parseInputs(I,varargin);

%get node of insterest
node = get(T,id);

if node.generation < gMax && numel(node.idx)>kMax
    
    
    %vectorize the image data (nPix X nLayers) and normalize to be between 0-1
    %(This effectively means that each layer is wieghted equally)
    data = zeros(numel(node.idx),size(I,4));
    S=size(I); if length(S)<3; S(3)=1; end;
    for i=1:size(I,4)
        data(:,i) = mat2gray(I(node.idx+(i-1)*prod(S(1:3))));
    end
    
    %Determine if image should be divided into two or more classes
    k=2;
    [inds,mu] = kmeans(data,k); %initial division by 2
    dist=norm(mu(1,:)-mu(2,:));
    test = max([mean(std(data(inds==1,:))) mean(std(data(inds==2,:)))]);
    
    if dist>test %In this case, subdivide into at least two more classes
        %initialize J
        Jold=0;
        J = getFishersRatio(data,inds,mu);
        %Determine how many subclasses to make (Run until J decreases)
        while J>Jold && k<=kMax
            Jold=J;
            k=k+1;
            inds_old=inds;
            mu_old = mu;
            [inds,mu] = kmeans(data,k);
            J = getFishersRatio(data,inds,mu);
        end
        %Once J decreased, use the last generations
        k=k-1;
        inds = inds_old;
        
        %add nodes to tree and recursive call to get all leaves of each of
        %these branches.
        if verbose
            disp(['Adding ' num2str(k) ' nodes to generation ' num2str(node.generation)]);
        end
        for i=1:k
            clear newnode
            newnode.idx=node.idx(inds==i);
            newnode.generation=node.generation+1;
            [T, newID] = T.addnode(id,newnode);
            T =  kMeansTreeSegmentation(I,'T',T,'id',newID,'kMax',kMax,'gMax',gMax,'verbose',verbose);
            
        end
        %make the tree the output
        output = T;
        
        
    else %otherwise output the original tree
        output = T;
    end
else %Output the original tree if max number of generations has been passed
    output = T;
end

%Make the final label map (This only runs after all nodes have been added)
if node.generation ==0
    clear output
    ids = findleaves(T);
    nLabels=numel(ids); %Number of labels (i.e. classes)
    S=size(I); if length(S)<3; S(3)=1; end;
    if nLabels<=2^8
        labels = zeros(S(1:3),'uint8');
    elseif nLabels<=2^16
        labels = zeros(S(1:3),'uint16');
    elseif nLabels<=2^32
        labels = zeros(S(1:3),'uint32');
    elseif nLabels<=2^64
        labels = zeros(S(1:3),'uint64');
    else
        warning('Too many labels!')
        labels = zeros(S(1:3));
    end
    
    for i=1:length(ids)
        node = get(T,ids(i));
        labels(node.idx)=i-1;
        
    end
    
    output.labels=labels;
    output.tree = T;
    output.nLabels=nLabels;
    output.nodeIds = ids;
    
end



end

function J = getFishersRatio(data,inds,mu)
k=size(mu,1);
MU = repmat(mean(mu),[size(mu,1) 1]); %Mean of centroids (accross classes)
B = sqrt( (sum((mu-MU).^2 )  )./(k-1)  ); %interclass dispersion vector
W = zeros(k,size(data,2)); %initialize intraclass dispersions matrix
for i=1:k
    rows = inds==i; %get the rows of interest
    W(i,:) = std(data(rows,:)); %compute dispersion (ie STD) for the ith (of k) class
end
W=mean(W); %intraclass dispersion vector

J = mean(B./W);

end

function [T,id,kMax,gMax,verbose]=parseInputs(I,args)
kMax = 2;
gMax = 6;
verbose = false;
if ~isempty(args)
    for i=1:2:length(args)
        switch args{i}
            case 'T'
                T = args{i+1};
            case 'mask'
                mask = args{i+1};
            case 'id'
                id = args{i+1};
            case 'kMax'
                kMax=args{i+1};
            case 'gMax'
                gMax = args{i+1};
            case 'verbose'
                verbose = args{i+1};
        end
    end
end

if ~exist('T','var') %This is the case when the algorithm is starting, need to initialize the tree
    if ~exist('mask','var')
        S=size(I); if length(S)<3; S(3)=1; end;
        idx = 1:prod(S(1:3));
    else
        idx=find(mask);
    end
    T.idx = idx;
    T.generation=0;
    T=tree(T);
    id = 1;
end

end