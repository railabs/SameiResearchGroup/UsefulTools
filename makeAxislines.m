% Copyright 20xx - 2019. Duke University
function [ h ] = makeAxislines(a, limits)
%This function draws 3D axis lines going through the origin.
%   a is the handle to the axis you want to draw make the axis on
%   limits is a 3x2 matrix with the limits of the axis lines:
%               limits=[xmin xmax; ymin ymax; zmin zmax];

Acolor=[.5 .5 .5]; %color of the axis lines
Tbuff=.1; %percent past the end of each line to draw the text

axes(a);
h.lines(1)=line([limits(1,1) limits(1,2)],[0 0],[0 0],'LineStyle','-','color',Acolor);
h.lines(2)=line([0 0],[limits(2,1) limits(2,2)],[0 0],'LineStyle','-','color',Acolor);
h.lines(3)=line([0 0],[0 0],[limits(3,1) limits(3,2)],'LineStyle','-','color',Acolor);
h.text(4)=text(limits(1,2)+limits(1,2)*Tbuff,0,0,'x','HorizontalAlignment','center','color',Acolor);
h.text(5)=text(0,limits(2,2)+limits(2,2)*Tbuff,0,'y','HorizontalAlignment','center','color',Acolor);
h.text(6)=text(0,0,limits(3,2)+limits(3,2)*Tbuff,'z','HorizontalAlignment','center','color',Acolor);



end

