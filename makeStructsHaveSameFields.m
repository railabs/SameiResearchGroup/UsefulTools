% Copyright 20xx - 2019. Duke University
function [str1, str2] = makeStructsHaveSameFields(str1,str2)

%get the field names
names1=fieldnames(str1);
names2=fieldnames(str2)';

for i=1:length(names1)
    for j=1:length(names2)
        
        if ~isfield(str2,names1{i})
            str2(1).(names1{i})=[];
        end
        
        if ~isfield(str1,names2{j})
            str1(1).(names2{j})=[];
        end
        
        
    end
end


end