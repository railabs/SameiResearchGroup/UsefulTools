% Copyright 20xx - 2019. Duke University
classdef ourClassTemplate < handle
    %This is a template that can be used as a starting point of defining a
    %new matlab class. It includes most of the common building blocks you
    %would use. For simple classes, some of the blocks are not needed. For
    %a complex user interface, you'll need most of these blocks.
    
    %Normal property block
    properties
        %This is where you list the property names of your class. Notice
        %how you can also define a default value for a property here in the
        %property block. If you dont add any attribute tags for a property
        %block, all the defaults are used (see documenation for what those
        %are)
        property1
        property2 = 'a string';
    end
    
    %Dependent property block
    properties (Dependent = true)
        %Here is another property block. This one sets the dependent
        %attribute to be true. A dependent property means that its value is
        %calculated every time its needed, i.e., its not saved in memory
        %but is computed based on other properties only when needed.
        %Whenever you define a dependent property, you have to define a get
        %method for it. That get method will presumably do some calculation
        %based on other properties of your class. For example, say you're
        %making a circle class. A circle is defined by 1 property, its
        %radius. But it can also be described by other properties such as
        %the diameter, area, or circumference. Those other properties are
        %really just dependent on the radius. So you would define a radius
        %property, and then a series of dependent properties. Each
        %dependent property would have a get method that returns its value
        %(e.g., diameter) based on the current value of the object's
        %radius.
        ourDependentProperty
    end
    
    %Constant property block.
    properties (Constant = true)
        %Is what it sounds like. Constant properties can never change once
        %the object is created. Use constant properties for things that
        %will always be the same across ALL instances of your class
        ourConstantProperty = 'This String Can Never Change!';
        AvagadrosNumber = 6.0221409e+23;
    end
    
    %Immutable properties
    properties (SetAccess = immutable)
        %Immuatable properties are ones that can only be set once, during
        %construction, and then can never change. Use immutable properties
        %for things that you want to remain constant for a specific
        %instance of your class but could change across different
        %instances.
        objectCreationTime
    end
    
    %Events block
    events
        %Events are basically "messages" that your object can transmit.
        %Other objects can listen for those events and execute code
        %accordingly. For example, say you have a graphical interface that
        %displays the properties of ourObject. You could listen for changes
        %to property1 and update the text on the interface with the new
        %value of property1. The important functions to know about for
        %using events are addlistener() (i.e., receive messages) and notify
        %(i.e., broadcast an event).
        property1Updated
    end
    
    %Methods block. 
    methods
        %These are all the functions that can be run using your
        %class. Also there a is a special method called a constructor which
        %is the function used to create a new instance of your class. Every
        %Matlab class needs a contructor!
        
        %Constructor
        function ourObject=ourClassTemplate(property1)
            %This is the code that is run to create a new instance of your
            %class (i.e., a new object). The number of inputs can vary
            %depending on the nature of your class but the output should
            %always be the handle to the object being created. Matlab knows
            %this is the constructor becuase the name of the function
            %matches the name of the class (and the .m file name)
            
            %Set property 1
            ourObject.property1=property1;
            
            %Add listener for future changes to property 1
            addlistener(ourObject,'property1Updated',@ourObject.handlePropEvents);
            
            %Set our immutable property, objectCreationTime
            ourObject.objectCreationTime = datetime;
        end
        
        %Get method
        function prop = get.property1(ourObject)
            %this code will run anytime someone tries to access the value
            %of property1. As currently written, this method is not needed
            %since all it does is return the value of property one. You
            %don't need to implement a get method to do just this. However,
            %if you need other things to happen anytime someone accesses
            %this property, then you can put that code here.
            
            %Gets the value of property1
            prop = ourObject.property1;
            
        end
        
        %set method
        function set.property1(ourObject,newValue)
            %This code will run anytime someone tries to set property 1
            %(e.g., ourObject.property1 = newValue);
            
            %Update the property value
            ourObject.property1=newValue;
            
            %Transmit a message saying to all listeners that property1 was
            %updated
            notify(ourObject,'property1Updated')
        end
        
        %Get method for a dependent property
        function ourDependentProperty = get.ourDependentProperty(ourObject)
            
            %Here ourDepdendentProperty is defined as 2 times property1. 
            ourDependentProperty = ourObject.property1*2; 
            
        end
        
        %Copy method
        function copiedObject = copy(ourObject)
            %Copy method (only used for handle-type classes). Normal routine in
            %a copy method is to call the class constructor and make a new
            %object with the same property values as the object you're copying
            %from
            
            %Call class constructor
            copiedObject = ourClassTemplate(ourObject.property1);
        end
        
        %Handle events function
        function handlePropEvents(ourObject,src,evnt,varargin)
            %This is a kind of catch-all function which will run when an
            %event being listened for is heard.
            switch evnt.EventName
                case 'property1Updated'
                    %This code will run anytime property1 is updated
                    disp('property1 was updated! I''m a good listener');
            end
        end
        
    end

end

%Callbacks for graphical interface components
function callbacks(hObject,evnt,ourObject)
%If you're making a GUI, you'll want to add callback functions to the
%interactive elements (e.g., buttons). You can put that callback code in
%this function. I (Justin) tend to use minimal code within the callbacks
%and try to put the important part of the code as methods of hte class.
%Thus usually my callbacks just call appropriate methods. This has the
%long-term effect of making it easier to drive the GUI from the command
%line (which is very very useful in a lot of cases).
switch hObject
    %This would switch over the handles to the graphics objects you've put
    %into your GUI. Thus you dont have to write a new callback function for
    %each graphical element. Just use this single function.
end
end