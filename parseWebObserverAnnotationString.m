% Copyright 20xx - 2019. Duke University
function out = parseWebObserverAnnotationString(str)
%This function converts the string that is saved in Taylor R's web-based
%observer studies when a user makes an annotation. The string contains all
%the x,y,z information about the click and other meta data but is just
%saved as a string in the database so this function converts that string
%into a useful structure.

%Here's an example of what one of those strings looks like"
% [{"x":"207.66","y":"341.33","z":"17","z_pos":"149","confidence":"50","timeStamp":1494073465242,"zoom":"1.68","ww":"195.00","wc":"111.00","highlight":true,"active":false}]
%define the fields to look for
fields = {'x','y','z','z_pos','confidence','timeStamp','zoom','ww','wc'};


%Check case of empty string (in this case an empty array is returned)
if strcmp(str,'[]')
    out = [];
else
    
    %Get the number of annotations (each annotation is enclosed in curly brace)
    k = strfind(str,'{');
    k2 = strfind(str,'}');
    if length(k)~=length(k2)
        warning('Problem with the number of annotations')
    end
    nAnnotations = length(k);
    
    %loop over the annotations
    for i=1:nAnnotations
        Astr = str(k(i):k2(i)); %Now we have a substring of only the info about a single annotation
        
        %loop over fields and extract values
        for j=1:length(fields)
            
            val = extractFieldValue(Astr,fields{j});
            out(i).(fields{j})=val;
            
            
        end
        
        
        
    end
end

end

function val = extractFieldValue(str,field)
%This helper function extracts the value from a specific field

%Define the pattern to search for (all fields are in quotations)
pat = ['"' field '":'];
%Find the pattern in the string
k = strfind(str,pat);
k=k(1);
%Trim the string to start at the value of that field
str = str(k+length(pat):end);
%find the ending character
k = strfind(str,',');
str = str(1:k-1);
%Remove quotes (if needed) around the value
str = strrep(str,'"','');
%Convert str to number
val = str2num(str);

end