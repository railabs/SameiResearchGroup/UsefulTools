% Copyright 20xx - 2019. Duke University
function [I,info]=readIodineSeries(input)
old_dir=pwd;

cd(input);

list=dir;

n=0;
for i=1:length(list)
    try
        info_t=dicominfo(list(i).name);
        im = double(dicomread(info_t));
        im=im*info_t.RescaleSlope+info_t.RescaleIntercept;
        
        
        
        try
            if info_t.InstanceNumber>0
                n=n+1;
                if n==1
                    I=im;
                    info=info_t;
                    slices=info_t.InstanceNumber;
                else
                    I(:,:,n)=im;
                    info(n)=info_t;
                    slices(n)=info_t.InstanceNumber;
                end
                
            end
        catch
            n=n-1;
        end
    end
    
    
end

[slices,IX] = sort(slices);
info=info(IX);
I=I(:,:,IX);

cd(old_dir);

end