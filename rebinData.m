% Copyright 20xx - 2019. Duke University
function [xbinned, ybinned, variance] = rebinData(x, y, edges,varargin)
%This function bins and averages the noisy scattered data

%Get optional input parameter
if length(varargin)==1
    extrap = varargin{1};
else
    extrap = 'extrap';
end

%compute histogram
try
    [~,xbinned,whichBin] = histcounts(x,edges);
catch
    [~,whichBin]= histc(x,edges);
    xbinned = edges;
end

%loop through histogram and average each bin
ybinned = zeros(size(xbinned));
variance = zeros(size(xbinned));
for i=1:length(xbinned)
    binMembers = y(whichBin == i);
    ybinned(i) = mean(binMembers); % this might create NaN. Interp done below takes care of this
    variance(i) = var(binMembers);
end

% NAN will appear after "mean" if binMember is empty, use values of neighboring bin instead
ybinned = interp1(find(~isnan(ybinned)),ybinned(~isnan(ybinned)),1:length(ybinned),'nearest',extrap);
variance = interp1(find(~isnan(variance)),variance(~isnan(variance)),1:length(variance),'nearest',extrap);
end