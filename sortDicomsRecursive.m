% Copyright 20xx - 2019. Duke University
function sortDicomsRecursive(input,output,varargin)
%This function recursively works through a folder (and subfolders) of
%DICOMS, and sorts them by series UID. It makes a copy of the files into
%the output folder (ignores hidden files)
%   Sytax.
%   sortDicomsRecursive(input,output) Run this if you don't want folder
%   names to be printed to the command line
%   sortDicomsRecursive(input,output,'Verbose',true) run this if you want
%   folder names to be printed to the command line (note that the recursive
%   nature of the function makes it difficult to get a sequential progress
%   update but printing to folder names as it steps through is somewhat
%   informative.

[verbose]=parseInputs(varargin);
if verbose; disp(input); end

%add file seperator to the end of the output directory if needed
if ~strcmp(output(end),filesep)
    output=[output filesep];
end

%add file seperator to the end of the input directory if needed
if ~strcmp(input(end),filesep)
    input=[input filesep];
end

%make output directory if it doesn't exist
if ~(exist(output,'dir'))
    mkdir(output)
end

%get all folders and files in the directory
list = dir(input);

%Loop over all files and folders
for i=1:length(list)
    if ~strcmp(list(i).name(1),'.') %this ignores hidden files (i.e., files that start with '.')
        
        if list(i).isdir %If its a directory recursively run this function
            sortDicomsRecursive([input list(i).name],output)
        else %Try to read in each file as a dicom
            
            try
                %read in the dicom
                info = dicominfo([input list(i).name]);
                
                %get the series UID
                SUID = info.SeriesInstanceUID;
                
                %Get the series Description
                try
                    SeriesDescription=info.SeriesDescription;
                    sep='_';
                catch
                    SeriesDescription = '';
                    sep='';
                end
                
                %Get the series number
                try
                    SeriesNumber = ['Series-' num2str(info.SeriesNumber)];
                    sep2 = '_';
                catch
                    SeriesNumber = '';
                    sep2 = '';
                end
                
                %Get study date
                try
                    StudyDate = info.StudyDate;
                    sep3 = '_';
                catch
                    StudyDate = '';
                    sep3 = '';
                end
                
                %Get the output folder name
                outdirname = [StudyDate sep3 SeriesNumber sep2 SeriesDescription sep 'UID-' SUID filesep];
                outdir = [output  outdirname];
                
                %Make the directory if needed
                if ~(exist(outdir,'dir'))
                    mkdir(outdir)
                end
                
                %Get the filename
                [PATHSTR,NAME,EXT] = fileparts(info.Filename);
                outname = [outdir  NAME EXT];
                
                %copy the file
                if exist(outname,'file')
                    warning('Multiple files with same name are being copied into same output folder')
                    disp(['Source file: ' input list(i).name]);
                    disp(['Output file: ' outname]);
                end
                copyfile(info.Filename,outname);
                
            catch ME
                test = 1;

            end
            
        end
    end
end


function [verbose]=parseInputs(args)
%defaults
verbose = false;
if ~isempty(args)
    for i=1:2:length(args)
        switch args{i}
            case 'Verbose'
                verbose = args{i+1};  
        end
    end
    
end


