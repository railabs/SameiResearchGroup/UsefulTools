function d = timestr2duration(times)

try
    fun = @(str) [str{1}(1:2) ':' str{1}(3:4) ':' str{1}(5:end)];
    d = duration(arrayfun(fun,times,'UniformOutput',false));
catch
    try
        ind = strcmp(times,'');
        times(ind)  = repmat({'000000.000000'},[sum(ind) 1]);
        d = duration(arrayfun(fun,times,'UniformOutput',false));
        d(ind) = NaT - NaT;
    catch
        times = [times(1:2) ':' times(3:4) ':' times(5:end)];
        d = duration(times);
    end
end

end