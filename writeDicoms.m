% Copyright 20xx - 2019. Duke University
function writeDicoms(output,I,info)

uid = dicomuid;

%Rescale and shift image data
slope=info(1).RescaleSlope;
intercept=info(1).RescaleIntercept;
I=int16(I./slope-intercept);

for i=1:size(I,3)
    
    %get the source dicom filename
    source=info(i).Filename;
    [pathstr, name, ext] = fileparts(source);
    
    info(i).SeriesInstanceUID=uid;
    
    %write the dicom file
    dicomwrite(I(:,:,i),[output filesep name ext],info(i))
    
end
end