% Copyright 20xx - 2019. Duke University
function writePSI( varargin )
%writes a .PSI file to visualize a point cloud in the AVIZO software
%package

%Ex.
% writePSI(x,y,z,ID,FileName) writes a psi file for points given by column
% vectors x, y, and z.
%
% writePSI(x,y,z,ID,FileName,ColumnName,Columndata...)


x=varargin{1}; y=varargin{2}; z=varargin{3};
ID=varargin{4}; FileName=varargin{5};

%make ID a vector of same length as the points
if prod(size(ID))==1
    ID=zeros(size(x))+ID;
end

%Create the default list of column titles
columnList={'x','y','z','Id'};

%Make matrix of data
data=[x y z ID];

%Add extra columns to header and data if necessary
if nargin >5
    for i=6:2:nargin
        column=i-1;
        columnList{column}=varargin{i};
        data=[data varargin{i+1}];
    end
end

%open the file
id=fopen(FileName,'w');

%print the file type
fprintf(id,'%s\n%s\n','# PSI Format 1.0','#');

%print the column names
for i=1:length(columnList)
    fprintf(id,'%s\n',['# column[' num2str(i-1) '] = "' columnList{i} '"']);
end
fprintf(id,'%s\n','');

%print the number of points
fprintf(id,'%u %u %u\n\n',length(x),1000,1000);

%print the bounding box (3x3 matrix that is ignored by Avizo)
fprintf(id,'%f %f %f\n',eye(3));
fprintf(id,'%s\n','');

%create formatting string for data printout
fspec='';
for i=1:length(columnList)
    fspec=[fspec '%f '];
end

%write out data
fprintf(id,[fspec '\n'],data');

%close the file
fclose(id);
